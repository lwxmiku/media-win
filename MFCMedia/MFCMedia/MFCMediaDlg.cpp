
// MFCMediaDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MFCMedia.h"
#include "MFCMediaDlg.h"
#include "afxdialogex.h"



#define  PATH "C:\\github\\test.mp4"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCMediaDlg 对话框



CMFCMediaDlg::CMFCMediaDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_MFCMEDIA_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCMediaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMFCMediaDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
END_MESSAGE_MAP()


// CMFCMediaDlg 消息处理程序

struct TestBit
{
	unsigned char a : 1;
	unsigned char b : 1;
	unsigned char c : 1;
	unsigned char d : 1;
	unsigned char e : 1;
	unsigned char f : 1;
	unsigned char g : 1;
	unsigned char h : 1;
};

BOOL CMFCMediaDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	//CAudioDevice* device = new CAudioDevice(1,48*1000,16);
	CMedia* media = CMedia::Open(PATH, this);

	media->Play();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFCMediaDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		DrawBitmap();
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFCMediaDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCMediaDlg::DrawBitmap()
{

}

void CMFCMediaDlg::AudioInformation(const AVChannelLayout srcLayout, const int srcRate, const AVSampleFormat srcFmt,
	AVChannelLayout* outLayout, int* outRate, AVSampleFormat* outFmt)
{
	*outLayout = srcLayout;
	*outRate = srcRate;
	*outFmt = srcFmt;
}

void CMFCMediaDlg::VideoInformation(const int srcWidth, const int srcHeight, const AVPixelFormat srcFmt, 
	int* outWidth, int* outHeight, AVPixelFormat* outFmt)
{
	*outWidth = srcWidth;
	*outHeight = srcHeight;
	*outFmt = srcFmt;
}

void CMFCMediaDlg::PlayImage(imgcvt::Image* img)
{

}

void CMFCMediaDlg::PlaySound(sndcvt::Sound* snd)
{

}

void CMFCMediaDlg::PlayText()
{

}

