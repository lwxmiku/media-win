
// MFCMediaDlg.h : 头文件
//

#pragma once

#include "Media/Media.h"



// CMFCMediaDlg 对话框
class CMFCMediaDlg : public CDialogEx , \
					 public IMediaDevice
{
// 构造
public:
	CMFCMediaDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFCMEDIA_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	//IMediaDevice
public:
	 void AudioInformation(const AVChannelLayout srcLayout, const int srcRate, const AVSampleFormat srcFmt,
		AVChannelLayout* outLayout, int* outRate, AVSampleFormat* outFmt);
	 void VideoInformation(const int srcWidth, const int srcHeight, const AVPixelFormat srcFmt, \
		int* outWidth, int* outHeight, AVPixelFormat* outFmt) ;

	 void PlayImage(imgcvt::Image* img) ;
	 void PlaySound(sndcvt::Sound* snd) ;
	 void PlayText() ;

protected:
	void DrawBitmap();

private:
	
};
