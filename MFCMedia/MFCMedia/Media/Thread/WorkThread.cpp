#include "Media/Thread/WorkThread.h"


CWorkThread::CWorkThread() :m_thread(NULL)
{

}

CWorkThread::~CWorkThread()
{

}

void CWorkThread::Start()
{
	DWORD id = 0;

	if (m_thread == NULL)
	{
		m_thread = CreateThread(NULL, 0, CWorkThread::ThreadProcess, this, 0, &id);
	}


}

DWORD WINAPI CWorkThread::ThreadProcess(LPVOID lpThreadParameter)
{
	CWorkThread* work = (CWorkThread*)lpThreadParameter;

	work->RunProcess();

	return 0;
}
