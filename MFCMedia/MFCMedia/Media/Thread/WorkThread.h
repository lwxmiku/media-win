#pragma once

#include <windows.h>


class CWorkThread
{
public:
	CWorkThread();
	~CWorkThread();

public:
	void Start();	//only called by WorkThreadProc function

protected:
	static DWORD WINAPI ThreadProcess(LPVOID lpThreadParameter);
protected:
	virtual void RunProcess() = 0;
protected:
	HANDLE m_thread;
};
