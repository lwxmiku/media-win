#pragma once

#include <vector>
#include <algorithm>
#include <windows.h>
using namespace  std;


template <typename product>
class CProducerConsumer
{
private:
	CONDITION_VARIABLE notEmpty;
	CONDITION_VARIABLE notFull;
	CRITICAL_SECTION   lockSection;

	const int maxCount;
	vector<product*> buff;

public:
	CProducerConsumer(const int count) : maxCount(count)
	{
		InitializeConditionVariable(&notEmpty);
		InitializeConditionVariable(&notFull);

		InitializeCriticalSection(&lockSection);
	}

	~CProducerConsumer()
	{
	
	}

public:
	void Push(product* prod)
	{
		size_t count = 0;

		EnterCriticalSection(&lockSection);

		count = buff.size();
		if (count >= maxCount)
		{
			SleepConditionVariableCS(&notFull, &lockSection, INFINITE);
		}

		buff.push_back(prod);

		LeaveCriticalSection(&lockSection);

		WakeConditionVariable(&notEmpty);
	}

	product* Pop()
	{
		size_t count = 0;
		product* prod = NULL;

		EnterCriticalSection(&lockSection);

		count = buff.size();
		if (count == 0)
		{
			SleepConditionVariableCS(&notEmpty,&lockSection,INFINITE);
		}

		auto it = buff.begin();
		prod = *it;
		buff.erase(it);

		count = buff.size();

		LeaveCriticalSection(&lockSection);

		if (count < maxCount)
			WakeConditionVariable(&notFull);

		return prod;
	}


	void PopAll(vector<product*>* ptr)
	{
		auto it = buff.begin(), \
			bit = buff.begin(), eit = buff.end();

		EnterCriticalSection(&lockSection);
		
		bit = buff.begin();
		eit = buff.end();
		for (it = bit; it != eit;it++)
		{
			ptr->push_back(*it);
			*it = NULL;
		}

		buff.clear();

		LeaveCriticalSection(&lockSection);
	}

};