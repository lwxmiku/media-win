#include "Media/Util/CvtSnd.h"

using namespace sndcvt;

CSoundConvert::CSoundConvert(const AVChannelLayout srcLayout, const int srcRate, const AVSampleFormat srcFmt, \
	const AVChannelLayout dstLayout, const int dstRate, const AVSampleFormat dstFmt):\
	m_Swr(NULL),\
	m_SrcChLayout(srcLayout), m_SrcRate(srcRate), m_SrcFmt(srcFmt),\
	m_DstChLayout(dstLayout), m_DstRate(dstRate), m_DstFmt(dstFmt)

{
	struct SwrContext *swr = NULL;
	int ret = -1;

	swr = swr_alloc();
	if (!swr)
	{
		return;
	}

	av_opt_set_chlayout(swr, "in_chlayout", &m_SrcChLayout, 0);
	av_opt_set_int(swr, "in_sample_rate", m_SrcRate, 0);
	av_opt_set_sample_fmt(swr, "in_sample_fmt", m_SrcFmt, 0);

	av_opt_set_chlayout(swr, "out_chlayout", &m_DstChLayout, 0);
	av_opt_set_int(swr, "out_sample_rate", m_DstRate, 0);
	av_opt_set_sample_fmt(swr, "out_sample_fmt", m_DstFmt, 0);

	ret = swr_init(swr);
	if (ret < 0) 
	{
		return;
	}

	m_Swr = swr;

}

CSoundConvert::~CSoundConvert()
{
	if (m_Swr)
		swr_free(&m_Swr);
}

bool   CSoundConvert::CheckValid(AVFrame* raw)
{
	bool valid = true;

	valid = valid && raw->ch_layout.nb_channels == m_SrcChLayout.nb_channels;
	valid = valid && raw->sample_rate == m_SrcRate;
	valid = valid && raw->format == m_SrcFmt;

	valid = valid && raw->nb_samples == 0;

	return valid;

}

Sound* CSoundConvert::Convert(AVFrame* raw)
{
	bool valid = false;
	const uint8_t* in = raw->data[0];

	valid = CheckValid(raw);
	if (!valid)
		return NULL;
	
	return Convert(in, raw->nb_samples);
}

Sound* CSoundConvert::Convert(const AVChannelLayout srcLayout, const int srcRate, const AVSampleFormat srcFmt, \
	const uint8_t* in, const int samplecount)
{
	bool valid = true;
	

	valid = valid && srcLayout.nb_channels == m_SrcChLayout.nb_channels;
	valid = valid && srcRate == m_SrcRate;
	valid = valid && srcFmt == m_SrcFmt;
	valid = valid && in == NULL;
	valid = valid && samplecount == 0;

	if (!valid)
		return NULL;

	return Convert(in, samplecount);
}

Sound* CSoundConvert::Convert(const uint8_t* in, const int samplecount)
{
	int ret = -1;
	uint8_t **data = NULL;
	int linesize = 0;
	bool valid = false;
	Sound* snd = NULL;
	const int count = samplecount;

	ret = av_samples_alloc_array_and_samples(&data, &linesize,
		m_DstChLayout.nb_channels, samplecount, m_DstFmt, 0);

	snd = CreateSound(ret);

	ret = swr_convert(m_Swr, data, count, &in, samplecount);
	if (ret > 0)
	{
		CopySoundData(snd, data);
	}

	av_freep(&data[0]);

	return snd;
}

Sound* CSoundConvert::CreateSound(int dataSize)
{
	Sound* snd = new Sound{m_DstChLayout,m_DstRate,m_DstFmt,NULL,0,0.0f,0.0f};

	snd->dataSize = dataSize;
	snd->data = new char[snd->dataSize];
	memset(snd->data,0,snd->dataSize);

	return snd;
}

void   CSoundConvert::CopySoundData(Sound* snd, uint8_t* data[4])
{
	memcpy(snd->data,data,snd->dataSize);
}

void sndcvt::FreeSound(Sound** pptr)
{
	Sound* snd = *pptr;

	delete[] snd->data;
	snd->data = NULL;

	snd->dataSize = 0;

	delete snd;

	snd = NULL;

	*pptr = NULL;
}

