#pragma once


extern "C"
{
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"
#include "libavutil/avutil.h"

#include "libswscale/swscale.h"
#include "libswresample/swresample.h"

#include "libavutil/imgutils.h"
#include "libavutil/rational.h"

#include "libavcodec/bsf.h"

#include "libavutil/opt.h"
#include "libavutil/channel_layout.h"
#include "libavutil/samplefmt.h"


}


AVFormatContext* OpenFormatContext(const char* path);
void CloseFormatContext(AVFormatContext** pptr);

int  FindAudioStreamIndex(AVFormatContext* ctx);
int  FindVideoStreamIndex(AVFormatContext* ctx);
int  FindSubtitleStreamIndex(AVFormatContext* ctx);

AVCodecContext*  CreateDecoder(AVStream* refstream);
void  FreeDecoder(AVCodecContext** pptr);

AVPacket* ReadPacket(AVFormatContext* ctx,bool *outeof,bool* outerror);
void	  FreePacket(AVPacket** pptr);

AVFrame*  DecodePacket(AVCodecContext* ctx,AVPacket* pkt);
void      FreeFrame(AVFrame** pptr);

unsigned long long FormatContextDuration(AVFormatContext* ctx);
void FormatContextSeek(AVFormatContext* ctx,  unsigned long long pos);






void		BitmapRoate180(char* data, const int width,const int height,const int bpp = 32);




#include <stdio.h>
#include <time.h>
#include <Windows.h>

static unsigned long long TimeMillisecond()
{
	SYSTEMTIME t1;
	struct tm temp;
	unsigned long long ms = 0;

	GetSystemTime(&t1);

	temp.tm_year = t1.wYear - 1900;
	temp.tm_mon = t1.wMonth - 1;
	temp.tm_mday = t1.wDay;
	temp.tm_hour = t1.wHour;
	temp.tm_min = t1.wMinute;
	temp.tm_sec = t1.wSecond;
	temp.tm_isdst = -1;

	time_t timestamp = mktime(&temp);

	ms = (unsigned long long) (timestamp * 1000);
	ms += t1.wMilliseconds;

	return ms;
}
