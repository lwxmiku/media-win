#pragma once

#include "Media/Util/MediaUtils.h"

namespace imgcvt
{
	struct Image
	{
		const int width, height;
		const AVPixelFormat fmt;

		char* data;
		int   dataSize;
	};

	void FreeImage(Image** pptr);




	class CImageConvert
	{
	public:
		CImageConvert(const int srcW, const int srcH, const AVPixelFormat srcFmt, \
							const int dstW, const int dstH, const AVPixelFormat dstFmt);
		~CImageConvert();

		Image* Convert(AVFrame* raw);
	private:
		Image* CreateImg();
		void   CopyImageData(Image* img, uint8_t* data[4]);

		bool CheckValid(AVFrame* raw);

	private:
		SwsContext* m_Sws;
		const int m_SrcWidth, m_SrcHeight;
		const AVPixelFormat m_SrcFmt;

		const int m_DstWidth, m_DstHeight;
		const AVPixelFormat m_DstFmt;
	};

};




