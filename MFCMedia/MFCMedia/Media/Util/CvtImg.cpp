#include "Media/Util/CvtImg.h"

using namespace imgcvt;

void imgcvt::FreeImage(Image** pptr)
{
	Image* img = *pptr;

	delete[] img->data; 
	img->data = NULL;

	img->dataSize = 0;

	delete img;
	img = NULL;

	*pptr = NULL;
}

CImageConvert::CImageConvert(const int srcW, const int srcH, const AVPixelFormat srcFmt, \
							const int dstW, const int dstH, const AVPixelFormat dstFmt) :\
							m_Sws(NULL),\
							m_SrcWidth(srcW), m_SrcHeight(srcH), m_SrcFmt(srcFmt),\
							m_DstWidth(dstW),m_DstHeight(dstH),m_DstFmt(dstFmt)
{
	SwsContext *ctx = NULL;

	ctx = sws_getContext(m_SrcWidth, m_SrcHeight, m_SrcFmt,
		m_DstWidth, m_DstHeight, m_DstFmt,
		SWS_BILINEAR, NULL, NULL, NULL);

	if (ctx)
	{
		m_Sws = ctx;
	}
}

CImageConvert::~CImageConvert()
{
	if(m_Sws) sws_freeContext(m_Sws);
}

Image* CImageConvert::CreateImg()
{
	int imgSize = -1;
	Image* img = new Image({ m_DstWidth ,m_DstHeight ,m_DstFmt ,NULL,0 });

	imgSize = av_image_get_buffer_size(m_DstFmt, m_DstWidth, m_DstHeight, 1);

	img->dataSize = imgSize;
	img->data = new char[img->dataSize];
	memset(img->data, 0, img->dataSize);

	return img;
}

bool CImageConvert::CheckValid(AVFrame* raw)
{
	bool valid = true;

	valid = valid && raw->width == m_SrcWidth; 
	valid = valid && raw->height == m_SrcHeight;
	valid = valid && raw->format == m_SrcFmt;

	return valid;
}

Image* CImageConvert::Convert(AVFrame* raw)
{
	int imgSize = -1;
	uint8_t* data[4] = { NULL };
	int		linesize[4] = { NULL };
	int		ret = -1;
	Image* img = NULL;
	bool valid = false;

	//check
	valid = CheckValid(raw);
	if (!valid)
		return NULL;


	//temp memory
	imgSize = av_image_alloc(data, linesize, m_DstWidth, m_DstHeight, m_DstFmt, 1);
	ret = sws_scale(m_Sws, \
		raw->data, raw->linesize, 0, raw->height, \
		data, linesize);
	
	//copy data into image
	img = CreateImg();
	CopyImageData(img,data);


	//free temp memory
	av_freep(&data[0]);


	return img;
}

void   CImageConvert::CopyImageData(Image* img, uint8_t* data[4])
{
	memcpy(img->data, data[0], img->dataSize);
}
