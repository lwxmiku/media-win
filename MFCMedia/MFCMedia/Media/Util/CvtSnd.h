#pragma once

#include "Media/Util/MediaUtils.h"

namespace sndcvt
{

	struct Sound
	{
		//audio param
		const AVChannelLayout layout;
		const int rate;
		const AVSampleFormat fmt;

		//data information
		char* data;
		int   dataSize;

		//time information
		double pts;
		double duration;
	};

	
	void FreeSound(Sound** pptr);
	

	class CSoundConvert
	{
	public:
		CSoundConvert(const AVChannelLayout srcLayout,const int srcRate, const AVSampleFormat srcFmt,\
			const AVChannelLayout dstLayout, const int dstRate, const AVSampleFormat dstFmt);
		~CSoundConvert();

		Sound* Convert(AVFrame* raw);
		Sound* Convert(const AVChannelLayout srcLayout, const int srcRate, const AVSampleFormat srcFmt,\
			const uint8_t* in,const int samplecount);

	private:
		Sound* CreateSound(int dataSize);
		void   CopySoundData(Sound* snd, uint8_t* data[4]);
	
		bool   CheckValid(AVFrame* raw);

		Sound* Convert(const uint8_t* in, const int samplecount);
	private:
		SwrContext* m_Swr;

		const  AVChannelLayout m_SrcChLayout;
		const int m_SrcRate;
		const AVSampleFormat m_SrcFmt;

		const  AVChannelLayout m_DstChLayout;
		const int m_DstRate;
		const AVSampleFormat m_DstFmt;
	};
};
