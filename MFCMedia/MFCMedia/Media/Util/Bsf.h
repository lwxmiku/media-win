#pragma once

#include "Media/Util/MediaUtils.h"

#include <vector>
using namespace std;

struct MediaFormatContext
{
	AVFormatContext* fmt;

	AVBSFContext* bsf;
};

struct Nalu
{
	char* data;
	int   dataSize;
};

MediaFormatContext* OpenMedia(const char* path);
void				CloseMedia(MediaFormatContext** pptr);

Nalu* ReadNalu(MediaFormatContext* ptr);
void  FreeNalu(Nalu** pptr);
