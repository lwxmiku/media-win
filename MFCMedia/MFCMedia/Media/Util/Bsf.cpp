#include "Media/Util/Bsf.h"

AVBSFContext* H264BsfCreate(AVCodecParameters* h264codec)
{
	const AVBitStreamFilter* bsf = av_bsf_get_by_name("h264_mp4toannexb");
	AVBSFContext* bsfCtx = NULL;
	int ret = -1;


	ret = av_bsf_alloc(bsf, &bsfCtx);
	if (0 != ret)
		goto fail;

	ret = avcodec_parameters_copy(bsfCtx->par_in, h264codec);
	if (ret < 0)
		goto fail;

	ret = av_bsf_init(bsfCtx);
	if (ret < 0)
		goto fail;


	return bsfCtx;

fail:
	if (bsfCtx)	av_bsf_free(&bsfCtx);
	bsfCtx = NULL;

	return NULL;
}

void H264BsfFree(AVBSFContext** pptr)
{
	AVBSFContext* ptr = *pptr;

	av_bsf_free(&ptr);
	*pptr = NULL;
}

AVPacket* Convert2Nalu(AVBSFContext* bsf, AVPacket* raw)
{
	AVPacket* nalupkt = av_packet_alloc();
	int ret = -1;

	ret = av_bsf_send_packet(bsf, raw);
	if (ret != 0)
		goto fail;

	ret = av_bsf_receive_packet(bsf, nalupkt);
	if (ret != 0)
		goto fail;

	return nalupkt;

fail:
	FreePacket(&nalupkt);
	return NULL;
}


MediaFormatContext* OpenMedia(const char* path)
{
	AVFormatContext* fmt = OpenFormatContext(path);
	AVBSFContext*     bsf = NULL;
	MediaFormatContext* ctx = NULL;
	int videoIndex = -1;

	if (fmt == NULL)
		goto fail;

	videoIndex = FindVideoStreamIndex(fmt);
	if (videoIndex == -1)
		goto fail;

	if (fmt->streams[videoIndex]->codecpar->codec_id != AV_CODEC_ID_H264)
		goto fail;

	bsf = H264BsfCreate(fmt->streams[videoIndex]->codecpar);
	if (bsf == NULL)
		goto fail;

	ctx = new MediaFormatContext;
	ctx->fmt = fmt;
	ctx->bsf = bsf;


	return ctx;

fail:
	return NULL;
}

void				CloseMedia(MediaFormatContext** pptr)
{

}

bool EqualArray(const unsigned char* src, const unsigned char* dst, const int count)
{
	int i = 0;
	bool eq = false;
	bool su = true;

	for (i = 0; i < count;i++)
	{
		eq = *(src+i) == *(dst+i);
		su = su &&  eq;
		if (!su)
			break;
	}

	return su;
}

void FindStartCode(const unsigned char* data, const int dataSize,int* outStart,int* outEnd)
{
	const unsigned char startcode0[4] = { 0x00,0x00,0x00,0x01 }, \
		startcode1[3] = { 0x00,0x00,0x01 };
	int offset = 0;
	bool eq0 = false, eq1 = false;
	int startCodeSize = 0;

	while (1)
	{
		if (offset >= dataSize)
			break;

		eq0 = EqualArray(startcode0, data + offset, 4);
		if (eq0)
		{
			startCodeSize = 4;
			break;
		}
		else
		{
			eq1 = EqualArray(startcode1, data + offset, 3);
			if (eq1)
			{
				startCodeSize = 3;
				break;
			}
			else
			{
				offset += 1;
			}
		}
	}
	

	if (startCodeSize != 0)
	{
		*outStart = offset;
		*outEnd = offset + startCodeSize;
	}
	else
	{
		*outStart = 0;
		*outEnd = 0;
	}
}

Nalu* NaluCreate(int size)
{
	Nalu* nalu = new Nalu{NULL,0};

	nalu->dataSize = size;
	nalu->data = new char[nalu->dataSize];

	memset(nalu->data,0, nalu->dataSize);

	return nalu;
}

Nalu* RemoveStartCode(const unsigned char* raw,const int rawSize,int* outSize)
{
	Nalu* rbsp = NULL;
	int start0 = 0, start1 = 0, end0 = 0, end1 = 0;
	int naluSize = 0;
	int pos = 0;

	//0x00 0x00 0x00 0x01 0xff 0xff 0x00 0x00 0x01

	FindStartCode(raw, rawSize, &start0,&end0);
	if (end0 - start0 != 0)
	{
		FindStartCode(raw + (end0 - start0), rawSize - (end0 - start0), &start1,&end1);
		if ((end1 - start1) != 0)
			naluSize = start1;
		else
			naluSize = rawSize - (end0 - start0);

		rbsp = NaluCreate(naluSize);
		memcpy(rbsp->data, raw + (end0 - start0), naluSize);

		pos = naluSize + (end0 - start0);
	}

	*outSize = pos;
	return rbsp;
}

vector<Nalu*> GetNalus(AVPacket* raw)
{
	Nalu* nalu = NULL;
	int offset = 0,length = 0;
	vector<Nalu*> nalus;

	while (1)
	{
		if (offset >= raw->size)
			break;

		length = 0;
		nalu = RemoveStartCode(raw->data + offset , raw->size - offset , &length);
		if (nalu)
		{
			nalus.push_back(nalu);
			offset += length;
		}
		else
			break;
	}

	return nalus;
}

Nalu* ReadNalu(MediaFormatContext* media)
{

	AVPacket* pkt = NULL, *nalupkt = NULL;
	bool bEof = false, bErr = false;
	vector<Nalu*> nalus;


	pkt = ReadPacket(media->fmt, &bEof, &bErr);
	if (pkt == NULL)
	{
		if (bEof)
			goto eoffail;
		if (bErr)
			goto errfail;

		goto readfail;
	}


	nalupkt = Convert2Nalu(media->bsf, pkt);
	if (nalupkt == NULL)
		goto cvtfail;

	nalus = GetNalus(nalupkt);
	av_packet_free(&nalupkt);

	
	return NULL;

readfail:
	if (pkt) FreePacket(&pkt);
	return NULL;

eoffail:
	if (pkt) FreePacket(&pkt);
	return NULL;

errfail:
	if (pkt) FreePacket(&pkt);
	return NULL;

cvtfail:
	FreePacket(&pkt);
	return NULL;
}

void  FreeNalu(Nalu** pptr)
{
	Nalu* ptr = *pptr;

	if (ptr->data) delete[] ptr->data;
	ptr->data = NULL;

	delete ptr;
	*pptr = NULL;
}