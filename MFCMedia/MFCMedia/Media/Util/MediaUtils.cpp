#include "Media/Util/MediaUtils.h"

AVFormatContext* OpenFormatContext(const char* path)
{
	AVFormatContext* ctx = NULL;
	int ret = -1;


	ret = avformat_open_input(&ctx, path, NULL, NULL);
	if (ret < 0)
		goto fail;

	ret = avformat_find_stream_info(ctx, NULL);
	if (ret < 0)
		goto fail;


	return ctx;

fail:
	if (ctx) CloseFormatContext(&ctx);

	return NULL;
}

void CloseFormatContext(AVFormatContext** pptr)
{
	AVFormatContext* ptr = *pptr;

	avformat_close_input(&ptr);
	ptr = NULL;

	*pptr = NULL;
}

int  FindAudioStreamIndex(AVFormatContext* ctx)
{
	int ret = -1;

	ret = av_find_best_stream(ctx,AVMEDIA_TYPE_AUDIO,-1,-1,NULL,0);
	if (ret < 0)
		return -1;

	return ret;
}

int  FindVideoStreamIndex(AVFormatContext* ctx)
{
	int ret = -1;

	ret = av_find_best_stream(ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
	if (ret < 0)
		return -1;

	return ret;
}

int  FindSubtitleStreamIndex(AVFormatContext* ctx)
{
	int ret = -1;

	ret = av_find_best_stream(ctx, AVMEDIA_TYPE_SUBTITLE, -1, -1, NULL, 0);
	if (ret < 0)
		return -1;

	return ret;
}

AVCodecContext*  CreateDecoder(AVStream* refstream)
{
	AVCodecContext* ctx = NULL;
	const AVCodec* codec = avcodec_find_decoder(refstream->codecpar->codec_id);
	int ret = -1;

	if (codec == NULL)
		goto fail;

	ctx = avcodec_alloc_context3(codec);
	if (ctx == NULL)
		goto fail;

	ret = avcodec_parameters_to_context(ctx, refstream->codecpar);
	if (ret < 0)
		goto fail;

	ret = avcodec_open2(ctx, codec, NULL);
	if (ret < 0)
		goto fail;

	return ctx;

fail:
	if (ctx) FreeDecoder(&ctx);
	return NULL;
}

void  FreeDecoder(AVCodecContext** pptr)
{
	AVCodecContext* ptr = *pptr;

	avcodec_free_context(&ptr);
	ptr = NULL;

	*pptr = NULL;
}

AVPacket* ReadPacket(AVFormatContext* ctx, bool *outeof, bool* outerror)
{
	AVPacket* pkt = av_packet_alloc();
	int ret = -1;
	bool fileeof = false, fileerr = false;

	ret = av_read_frame(ctx,pkt);
	if (ret >= 0)
	{
		*outeof = false;
		*outerror = false;
		return pkt;
	}
	else
	{
		if (ret == AVERROR_EOF)
			fileeof = true;
		else
			fileerr = true;
	}
	

	FreePacket(&pkt);

	*outeof = fileeof;
	*outerror = fileerr;
	return NULL;
}

void	  FreePacket(AVPacket** pptr)
{
	av_packet_free(pptr);
}

AVFrame*  DecodePacket(AVCodecContext* ctx, AVPacket* pkt)
{
	AVFrame* frm = av_frame_alloc();
	int ret = -1;
	bool err = false;

	ret = avcodec_send_packet(ctx, pkt);
	if (ret < 0)
		goto fail;

	while (1)
	{
		ret = avcodec_receive_frame(ctx, frm);
		if (ret >= 0)
			break;
		else
		{
			if (ret == AVERROR_EOF || ret == AVERROR(EAGAIN))
				break;
			else
			{
				err = true;
				break;
			}
				
		}
	}

	if (err)
		goto fail;

	return frm;
fail:
	if (frm) FreeFrame(&frm);
	return NULL;
}

void      FreeFrame(AVFrame** pptr)
{
	av_frame_free(pptr);
}



void		BitmapRoate180(char* data,const int width,const int height,const int bitbpp)
{
	const int bpp = bitbpp;
	const int image_width = width, image_height = height;
	char* pData = data;

	int index = bpp / 8;
	for (int h = 0; h < image_height / 2; h++)
	{
		for (int w = 0; w < image_width; w++)
		{
			const int iCoordM = index*(h*image_width + w);
			const int iCoordN = index*((image_height - h - 1)*image_width + w);
			unsigned char Tmp = pData[iCoordM];
			pData[iCoordM] = pData[iCoordN];
			pData[iCoordN] = Tmp;
			Tmp = pData[iCoordM + 1];
			pData[iCoordM + 1] = pData[iCoordN + 1];
			pData[iCoordN + 1] = Tmp;
			Tmp = pData[iCoordM + 2];
			pData[iCoordM + 2] = pData[iCoordN + 2];
			pData[iCoordN + 2] = Tmp;
		}
	}
}