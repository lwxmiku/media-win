#include "Media/Media.h"

CMedia::CMedia(AVFormatContext* fmtCtx, IMediaDevice* device) :\
						m_Ctx(NULL), m_Device(device),\
						m_Task(NULL)
{
	m_Ctx = new MediaContext({ fmtCtx,\
		-1,NULL,\
		-1,NULL,\
		-1,NULL,\
		 });

	Init();
}

CMedia::~CMedia()
{

}

void CMedia::Init()
{
	InitAudioCtx();
	InitVideoCtx();
	InitSubtitleCtx();

	InitTask();
}

void CMedia::InitTask()
{
	m_Task = new CTasks(m_Ctx, m_Device);
}

void CMedia::InitAudioCtx()
{
	int index = FindAudioStreamIndex(m_Ctx->fmtCtx);
	if (index < 0)
		return;

	m_Ctx->audioStreamIndex = index;
	m_Ctx->audioCtx = CreateDecoder(m_Ctx->fmtCtx->streams[index]);
}

void CMedia::InitVideoCtx()
{
	int index = FindVideoStreamIndex(m_Ctx->fmtCtx);
	if (index < 0)
		return;

	m_Ctx->videoStreamIndex = index;
	m_Ctx->videoCtx = CreateDecoder(m_Ctx->fmtCtx->streams[index]);

	AVPixelFormat fmt = (AVPixelFormat)m_Ctx->fmtCtx->streams[index]->codecpar->format;
	int width = m_Ctx->fmtCtx->streams[index]->codecpar->width, height = m_Ctx->fmtCtx->streams[index]->codecpar->height;
	
}

void CMedia::InitSubtitleCtx()
{
	int index = FindSubtitleStreamIndex(m_Ctx->fmtCtx);
	if (index < 0)
		return;

	m_Ctx->subtitleStreamIndex = index;
	m_Ctx->subtitleCtx = CreateDecoder(m_Ctx->fmtCtx->streams[index]);
}

CMedia* CMedia::Open(const char* path, IMediaDevice* device)
{
	AVFormatContext* fmt = NULL;
	CMedia*  media = NULL;

	fmt = OpenFormatContext(path);
	if (fmt == NULL)
		goto fail;

	media = new CMedia(fmt, device);

	return media;

fail:
	if (fmt) CloseFormatContext(&fmt);
	return NULL;
}

void CMedia::Play()
{
	m_Task->Play();
}

void CMedia::Stop()
{

}

void CMedia::Seek()
{

}