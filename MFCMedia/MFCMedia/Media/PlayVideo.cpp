#include "Media/PlayVideo.h"

/*

CPlayVideo::CPlayVideo(IMediaInterface* media, const int streamindex, const int maxCount):\
	m_refMedia(media),m_streamindex(streamindex),m_maxCount(maxCount),\
	m_frms(NULL),\
	m_lastest(NULL), m_ctrltime({NULL})
{
	m_frms = new CProducerConsumer<AVFrame>(maxCount);
}

CPlayVideo::~CPlayVideo()
{

}

void CPlayVideo::PushFrame(AVFrame* frm)
{
	m_frms->Push(frm);
}

void CPlayVideo::RunProcess()
{
	return Loop();
}

void CPlayVideo::Loop()
{
	while (1)
	{
		if (m_lastest == NULL)
		{
			m_lastest = m_frms->Pop();

			if (m_lastest->format == -1)
			{
				FreeFrame(&m_lastest);
				m_lastest = NULL;
			}

			continue;
		}

		PlayFrame(m_lastest);

		FreeFrame(&m_lastest);
		m_lastest = NULL;
	}
}

bool CPlayVideo::FirstPlay(AVFrame* frm)
{
	bool first = false;

	if (m_ctrltime.pts == 0 && \
		m_ctrltime.duration == 0 && \
		m_ctrltime.nowtime == 0 && \
		m_ctrltime.delta == 0)
	{
		RendererImage(frm);

		UpdateControlTime(frm->pts, frm->duration, m_refMedia->NowTime(), 0);

		first = true;
	}

	return first;
}


void CPlayVideo::PlayFrame(AVFrame* frm)
{
	const bool sync = true;//m_refMedia->SyncAudio();
	int waitms = 0;
	bool first = false;

	while (1)
	{
		first = FirstPlay(frm);
		if (first)
			break;

		if (sync)
			waitms = TryPlaySync(frm);
		else
			waitms = TryPlay(frm);
		
		if (waitms == 0)
			break;
		else
			Sleep(waitms / 2);
	}
}

int CPlayVideo::CheckPlayOver()
{
	const double tbms = TimeBase();
	const int64_t nextplaytime = m_ctrltime.nowtime + (m_ctrltime.duration * tbms) - m_ctrltime.delta;
	const int64_t nowtime = m_refMedia->NowTime();
	const bool over = nowtime >= nextplaytime;
	int remainms = 0;

	if (over)
		remainms = 0;
	else
		remainms = nextplaytime - nowtime;

	return remainms;
}

int CPlayVideo::TryPlay(AVFrame* frm)
{
	const int remainms = CheckPlayOver();

	if (remainms == 0)
	{
		NormalPlay(frm);
	}

	return remainms;
}

void CPlayVideo::DiscardVideo(AVFrame* frm)
{
	m_ctrltime.pts = frm->pts;
	m_ctrltime.duration = frm->duration;

	m_ctrltime.nowtime = m_refMedia->NowTime();
	m_ctrltime.delta = 0;
}

int CPlayVideo::TryPlaySync(AVFrame* frm)
{
	const FramePosition pos = CheckPosition(frm);
	int remainms = 0;

	if (POS_BEFORE == pos)
	{
		//discard
		DiscardVideo(frm);

		remainms = 0;
	}
	else if (POS_AFTER == pos)
	{
		//wait
		CheckAllVideo();
		remainms = 0;
	}
	else if (POS_CENTER == pos)
	{
		//wait
		remainms = CheckPlayOver();
		if (remainms == 0)
		{
			NormalPlay(frm);
		}
	}

	return remainms;
	
}

void CPlayVideo::CheckAllVideo()
{

}

void CPlayVideo::UpdateControlTime(int64_t pts, int64_t duration, int64_t nowtime, int delta)
{
	m_ctrltime.pts = pts;
	m_ctrltime.duration = duration;

	m_ctrltime.nowtime = nowtime;
	m_ctrltime.delta = delta;
}

void CPlayVideo::NormalPlay(AVFrame* frm)
{
	RendererImage(frm);

	int64_t nextplaytime = m_ctrltime.nowtime + (m_ctrltime.duration * TimeBase()) - m_ctrltime.delta;

	m_ctrltime.nowtime = m_refMedia->NowTime();
	m_ctrltime.delta = m_ctrltime.nowtime - nextplaytime;

	m_ctrltime.pts = frm->pts;
	m_ctrltime.duration = frm->duration;

	//check delta
	m_ctrltime.delta = m_ctrltime.delta >= 300 ? 0 : m_ctrltime.delta;

}


const double CPlayVideo::TimeBase()
{
	static const double tb = av_q2d(m_refMedia->TimeBase(m_streamindex));
	static const double tbms = 1000.0f * tb;

	return tbms;
}

FramePosition CPlayVideo::CheckPosition(AVFrame* frm)
{
	const double videopts = TimeBase() * (double(frm->pts));
	const double videoduration = TimeBase() * (double (frm->duration));
	double audiopts = 0.0f, audioduration = 0.0f;
	bool before = false, after = false;
	FramePosition pos = POS_NONE;

	m_refMedia->AudioTime(&audiopts, &audioduration);

	before = audiopts - (videopts + videoduration) >= 2.0f;
	after =  videopts - (audiopts + audioduration) >= 2.0f;

	if (before)
		pos = POS_BEFORE;
	else if (after)
		pos = POS_AFTER;
	else
		pos = POS_CENTER;

	return pos;
}


void CPlayVideo::RendererImage(AVFrame* frm)
{
	
}

*/