#pragma once

#include "Media/Task/Tasks.h"

#include "Media/Util/CvtImg.h"
#include "Media/Util/CvtSnd.h"

class IMediaDevice
{
protected:
	IMediaDevice() = default;
	virtual ~IMediaDevice() = default;
public:
	virtual void AudioInformation(const AVChannelLayout srcLayout,const int srcRate,const AVSampleFormat srcFmt,
		AVChannelLayout* outLayout,int* outRate, AVSampleFormat* outFmt) = 0;
	virtual void VideoInformation(const int srcWidth,const int srcHeight,const AVPixelFormat srcFmt,\
		int* outWidth,int* outHeight,AVPixelFormat* outFmt) = 0;

	virtual void PlayImage(imgcvt::Image* img) = 0;
	virtual void PlaySound(sndcvt::Sound* snd) = 0;
	virtual void PlayText() = 0;

};


class CMedia 
{
private:
	CMedia(AVFormatContext* fmtCtx, IMediaDevice* device);
public:
	~CMedia();
	static CMedia* Open(const char* path, IMediaDevice* device);


	void Play();
	void Stop();
	void Seek();

private:
	void Init();

	void InitAudioCtx();
	void InitVideoCtx();
	void InitSubtitleCtx();
	void InitTask();
	

protected:
	IMediaDevice* m_Device;
	MediaContext* m_Ctx;
	CTasks*       m_Task;
};

