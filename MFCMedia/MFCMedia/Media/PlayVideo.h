#pragma once

/*

#include "Media/MediaUtils.h"
#include "Media/MediaInterface.h"
#include "Media/Thread/ProducerConsumer.h"
#include "Media/Thread/WorkThread.h"

enum FramePosition
{
	POS_NONE,
	POS_BEFORE,
	POS_AFTER,
	POS_CENTER
};

class CPlayVideo :public CWorkThread
{
public:
	CPlayVideo(IMediaInterface* media, const int streamindex, const int maxCount);
	~CPlayVideo();

public:
	void PushFrame(AVFrame* frm);

protected:
	void RunProcess();
	void Loop();

protected:
	const double TimeBase();

	void PlayFrame(AVFrame* frm);

	bool FirstPlay(AVFrame* frm);
	void UpdateControlTime(int64_t pts, int64_t duration,int64_t nowtime,int delta);

	int TryPlay(AVFrame* frm);
	int TryPlaySync(AVFrame* frm);

	int CheckPlayOver();
	void NormalPlay(AVFrame* frm);
	
	FramePosition CheckPosition(AVFrame* frm);

	void DiscardVideo(AVFrame* frm);

	void CheckAllVideo();

	void RendererImage(AVFrame* frm);

protected:
	IMediaInterface* m_refMedia;
	const int m_streamindex;
	const int m_maxCount;
	CProducerConsumer<AVFrame>* m_frms;

	AVFrame* m_lastest;
	ControlTime m_ctrltime;
};

*/