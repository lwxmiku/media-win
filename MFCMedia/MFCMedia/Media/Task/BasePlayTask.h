#pragma once

#include "Media/Thread/WorkThread.h"
#include "Media/Task/ITaskAccess.h"
#include "Media/Thread/ProducerConsumer.h"


class CBasePlayTask 
{
public:
	CBasePlayTask(ITaskAccess* access, const int streamindex,const int maxCount);
	~CBasePlayTask();

	void PushFrame(AVFrame* frm);

protected:
	ITaskAccess* m_Access;
	const int m_StreamIndex;

	CProducerConsumer<AVFrame>* m_Buff;

	PlayedTimeClock m_Time;
	double m_TimeBase;
};
