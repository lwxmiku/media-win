#include "Media/Task/Tasks.h"
#include "Media/Media.h"

CTasks::CTasks( MediaContext* ctx, IMediaDevice* device):m_Ctx(ctx),m_Device(device),\
								m_ImgCvt(NULL),m_SndCvt(NULL),\
								m_Demux(NULL), \
								m_DecodeAudio(NULL), m_PlayAudio(NULL), \
								m_DecodeVideo(NULL), m_PlayVideo(NULL), \
								m_DecodeSubtitle(NULL), m_PlaySubtitle(NULL),\
								m_State(STATE_NONE)
{
	Init();
}

CTasks::~CTasks()
{

}

void CTasks::Init()
{
	InitConvert();
	InitTask();
}

void CTasks::InitTask()
{
	m_Demux = new CDemuxTask(this);

	//audio
	if (m_Ctx->audioCtx)
	{
		m_DecodeAudio = new CDecodeTask(this, m_Ctx->audioStreamIndex);
		m_PlayAudio = new CPlayAudioTask(this, m_Ctx->audioStreamIndex);
	}

	//video
	if (m_Ctx->videoCtx)
	{
		m_DecodeVideo = new CDecodeTask(this, m_Ctx->videoStreamIndex);
		m_PlayVideo = new CPlayVideoTask(this, m_Ctx->audioStreamIndex);
	}

	//subtitle
	if (m_Ctx->subtitleCtx)
	{
		m_DecodeSubtitle = new CDecodeTask(this, m_Ctx->subtitleStreamIndex);
		m_PlaySubtitle = new CPlaySubtitleTask(this, m_Ctx->audioStreamIndex);
	}
}

void CTasks::InitConvert()
{
	InitSoundConvert();
	InitImageConvert();
}

void CTasks::InitImageConvert()
{
	if (!m_Ctx->videoCtx)
		return;

	AVStream* video = m_Ctx->fmtCtx->streams[m_Ctx->videoStreamIndex];
	const int width = video->codecpar->width;
	const int height = video->codecpar->height;
	const  AVPixelFormat fmt =(AVPixelFormat) video->codecpar->format;

	int dstW = 0, dstH = 0;
	AVPixelFormat dstFmt = AV_PIX_FMT_NONE;

	m_Device->VideoInformation(width, height, fmt, &dstW, &dstH, &dstFmt);

	m_ImgCvt = new imgcvt::CImageConvert(width,height,fmt,dstW,dstH,dstFmt);
}

void CTasks::InitSoundConvert()
{
	if (!m_Ctx->audioCtx)
		return;

	AVStream* audio = m_Ctx->fmtCtx->streams[m_Ctx->audioStreamIndex];
	const AVChannelLayout layout = audio->codecpar->ch_layout;
	const int rate = audio->codecpar->sample_rate;
	const AVSampleFormat fmt = (AVSampleFormat)audio->codecpar->format;

	AVChannelLayout dstLayout;
	int dstRate = 0;
	AVSampleFormat dstFmt = AV_SAMPLE_FMT_NONE;

	m_Device->AudioInformation(layout, rate, fmt, &dstLayout, &dstRate, &dstFmt);

	m_SndCvt = new sndcvt::CSoundConvert(layout,rate,fmt,dstLayout,dstRate,dstFmt);
}

void CTasks::Play()
{
	switch (m_State)
	{
	case STATE_NONE:
		Start();
		m_State = STATE_PLAY;
		break;
	case STATE_PLAY:
		break;
	case STATE_PAUSE:
		break;
	case STATE_STOP:
		break;
	default:
		break;
	}

	
}


void CTasks::Start()
{
	if (m_PlayAudio)
		m_PlayAudio->Start();

	if (m_PlayVideo)
		m_PlayVideo->Start();

	if (m_PlaySubtitle)
		m_PlaySubtitle->Start();

	if (m_DecodeAudio)
		m_DecodeAudio->Start();

	if (m_DecodeVideo)
		m_DecodeVideo->Start();

	if (m_DecodeSubtitle)
		m_DecodeSubtitle->Start();

	m_Demux->Start();
}

void CTasks::Resume()
{

}