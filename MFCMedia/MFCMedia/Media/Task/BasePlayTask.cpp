#include "Media/Task/BasePlayTask.h"

CBasePlayTask::CBasePlayTask(ITaskAccess* access, const int streamindex,const int maxCount):\
m_Access(access),m_StreamIndex(streamindex),\
	m_Buff(NULL),\
	m_Time({0.0f,0.0f,0,0}),m_TimeBase(0.0f)
{
	m_Buff = new CProducerConsumer<AVFrame>(maxCount);

	m_TimeBase = m_Access->StreamTimeBase(m_StreamIndex);
}

CBasePlayTask::~CBasePlayTask()
{

}

void CBasePlayTask::PushFrame(AVFrame* frm)
{
	m_Buff->Push(frm);
}