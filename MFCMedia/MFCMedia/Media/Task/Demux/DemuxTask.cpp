#include "Media/Task/Demux/DemuxTask.h"

CDemuxTask::CDemuxTask(ITaskAccess* access) : mAccess(access),mPkt(NULL)
{

}

CDemuxTask::~CDemuxTask()
{

}

void CDemuxTask::RunProcess()
{
	return Loop();
}

void CDemuxTask::Loop()
{
	AVFormatContext* refFmt = mAccess->FormatContext();
	bool fileeof = false, fileerr = false;

retry:
	while (1)
	{
		if (mPkt == NULL)
		{
			fileeof = false;
			fileerr = false;

			mPkt = ReadPacket(refFmt, &fileeof, &fileerr);
			if (mPkt == NULL)
			{
				if (fileeof)
				{
					//
					break;
				}

				if (fileerr)
					break;
			}
		}
		
		mAccess->PushPakcet(mPkt);
		mPkt = NULL;
	}

	if (fileeof)
	{
		while (1)
		{
			Sleep(100);
		}
	}
}