#pragma once

#include "Media/Util/MediaUtils.h"
#include "Media/Thread/WorkThread.h"
#include "Media/Task/ITaskAccess.h"


class CDemuxTask :public CWorkThread
{
public:
	CDemuxTask(ITaskAccess* access);
	~CDemuxTask();

protected:
	void RunProcess();
	void Loop();

protected:
	ITaskAccess* mAccess;
	AVPacket* mPkt;
};
