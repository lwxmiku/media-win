#include "Media/Task/Tasks.h"

//demux
AVFormatContext* CTasks::FormatContext()
{
	return m_Ctx->fmtCtx;
}

void CTasks::PushPakcet(AVPacket* pkt)
{
	if (pkt->stream_index == m_Ctx->audioStreamIndex)
	{
		m_DecodeAudio->PushPacket(pkt);
	}
	else if (pkt->stream_index == m_Ctx->videoStreamIndex)
	{
		m_DecodeVideo->PushPacket(pkt);
	}
	else if (pkt->stream_index == m_Ctx->subtitleStreamIndex)
	{
		m_DecodeSubtitle->PushPacket(pkt);
	}
	else
	{
		FreePacket(&pkt);
		pkt = NULL;
	}

}

//decode
AVCodecContext* CTasks::CodecContext(const int streamindex)
{
	if (streamindex == m_Ctx->audioStreamIndex)
	{
		return m_Ctx->audioCtx;
	}
	else if (streamindex == m_Ctx->videoStreamIndex)
	{
		return m_Ctx->videoCtx;
	}
	else if (streamindex == m_Ctx->subtitleStreamIndex)
	{
		return m_Ctx->subtitleCtx;
	}

	return NULL;
}

void CTasks::PushFrame(AVFrame* frm, const int streamIndex)
{
	if (streamIndex == m_Ctx->audioStreamIndex)
	{
		m_PlayAudio->PushFrame(frm);
	}
	else if (streamIndex == m_Ctx->videoStreamIndex)
	{
		m_PlayVideo->PushFrame(frm);
	}
	else if (streamIndex == m_Ctx->subtitleStreamIndex)
	{
		m_PlaySubtitle->PushFrame(frm);
	}
	else
	{
		FreeFrame(&frm);
		frm = NULL;
	}
}


//convert
imgcvt::CImageConvert* CTasks::ImageConvert()
{
	return m_ImgCvt;
}

sndcvt::CSoundConvert* CTasks::SoundConvert()
{
	return m_SndCvt;
}

double CTasks::StreamTimeBase(const int streamindex)
{

	AVRational tb = m_Ctx->fmtCtx->streams[streamindex]->time_base;
	return av_q2d(tb);
}

unsigned long long CTasks::Now()
{
	return TimeMillisecond();
}

void CTasks::RendererImage()
{

}

void CTasks::PlaybackSound()
{

}

void CTasks::ShowText()
{

}

bool CTasks::SynchronizationTime(double* outPts, double* outDuration)
{
	bool sync = Synchronization();

	if (!sync)
	{
		*outPts = 0.0f;
		*outDuration = 0.0f;
	}
	else
	{
		m_PlayAudio->AudioTime(outPts, outDuration);
	}

	return sync;
}

bool CTasks::Synchronization()
{
	return (m_Ctx->audioCtx != NULL);
}