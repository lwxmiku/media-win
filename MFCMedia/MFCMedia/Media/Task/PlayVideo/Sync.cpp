#include "Media/Task/PlayVideo/PlayVideoTask.h"

void  CPlayVideoTask::SyncPlay()
{
	Image* img = PopReadyImage();
	bool first = false;
	unsigned int remain = 0;
	bool nexFront = false, nexBack = false;

	

	if (img == NULL)
		return;

	while (1)
	{
		first = FirstPlay(img);
		if (first)
			break;


		CheckImageIsValid(img, &nexFront, &nexBack);
		remain = RemainTime();

		if ((!nexFront && !nexBack))
		{
			if (remain == 0)
			{
				RealPlay(img);
				UpdateTime(img);
				break;
			}
			else
			{
				Sleep(remain/2);
			}
		}
		else if (nexFront)
		{
			Discard(img);
			break;
		}
		else if (nexBack)
		{
			if (remain == 0)
			{
				Discard(img);
				break;
			}
			else
			{
				Sleep(1);
			}
				
		}
		else
		{
			//error
			
		}
		
	}

	FreeImage(&img);
}

void CPlayVideoTask::CheckImageIsValid(Image* img, bool* outFront, bool* outBack)
{
	double pts = 0.0f, duration = 0.0f;
	const double imgpts = img->pts * m_TimeBase, imgdur = img->duration * m_TimeBase;
	bool front = false, back = false;
	

	m_Access->SynchronizationTime(&pts, &duration);

	front = (imgpts) > pts + duration;
	back = imgpts + imgdur < pts;

	
	*outFront = front;
	*outBack = back;
}

void CPlayVideoTask::CheckCurrentIsValid(bool* outFront, bool* outBack)
{
	double pts = 0.0f, duration = 0.0f;
	const double imgpts = m_Time.pts, imgdur = m_Time.duration;
	bool front = false, back = false;


	m_Access->SynchronizationTime(&pts, &duration);

	front = (imgpts) > pts + duration;
	back = imgpts + imgdur < pts;


	*outFront = front;
	*outBack = back;
}

void CPlayVideoTask::Discard(Image* img)
{
	m_Time.pts = img->pts;
	m_Time.duration = img->duration;
	m_Time.ms = m_Access->Now();
	m_Time.delta = 0;
}