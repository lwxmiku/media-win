#include "Media/Task/PlayVideo/PlayVideoTask.h"

void CPlayVideoTask::NonSyncPlay()
{
	Image* img = PopReadyImage();
	bool first = false;
	unsigned int remain = 0;

	if (img == NULL)
		return;

	while (1)
	{
		first = FirstPlay(img);
		if (first)
			break;

		remain = RemainTime();
		if (remain == 0)
		{
			RealPlay(img);
			UpdateTime(img);
			break;
		}
		else
		{
			Sleep(remain / 2);
		}
		
	}

	FreeImage(&img);
}