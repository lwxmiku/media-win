#pragma once

#include "Media/Util/MediaUtils.h"
#include "Media/Thread/WorkThread.h"
#include "Media/Task/ITaskAccess.h"
#include "Media/Thread/ProducerConsumer.h"

#include "Media/Task/BasePlayTask.h"

#define  VIDEO_MAX_FRAME_COUNT 10
#define  IMAGE_MAX_COUNT 3
#define  FORCE_MAX_DELTA 100

class CPlayVideoTask : public CWorkThread , 
	public CBasePlayTask
{
public:
	CPlayVideoTask(ITaskAccess* access, const int streamindex);
	~CPlayVideoTask();

	
	struct Image
	{
		int64_t pts;
		int64_t duration;
	};

protected:
	void RunProcess();
	void Loop();

	//
private:
	//
	bool SetReadyImage(AVFrame* frm);
	Image* PopReadyImage();
	void FreeImage(Image** pptr);

	//time
	unsigned int  RemainTime();
	void		  UpdateTime(Image* img);

	//play
	bool FirstPlay(Image* img);
	void RealPlay(Image* img);

private:
	void NonSyncPlay();
	void SyncPlay();

	void CheckImageIsValid(Image* img,bool* outFront,bool* outBack);
	void CheckCurrentIsValid(bool* outFront, bool* outBack);
	void Discard(Image* img);
private:
	
private:
	AVFrame* m_Frm;
	vector<Image*> m_imgs;
};

