#include "Media/Task/PlayVideo/PlayVideoTask.h"

CPlayVideoTask::CPlayVideoTask(ITaskAccess* access, const int streamindex):\
								CBasePlayTask(access, streamindex,VIDEO_MAX_FRAME_COUNT),\
								m_Frm(NULL)
{
	
}

CPlayVideoTask::~CPlayVideoTask()
{

}


void CPlayVideoTask::RunProcess()
{
	return Loop();
}

void CPlayVideoTask::Loop()
{
	const bool sync = m_Access->Synchronization();
	bool ready = false;

	while (1)
	{
		if (m_Frm == NULL)
		{
			m_Frm = m_Buff->Pop();
			continue;
		}


		//invalid frame
		if (m_Frm->format == -1)
		{
			FreeFrame(&m_Frm);
			m_Frm = NULL;
			continue;;
		}

		//convert and store frame
		ready = SetReadyImage(m_Frm);
		if (ready)
		{
			FreeFrame(&m_Frm);
			m_Frm = NULL;	
			continue;
		}
		

		//play image
		if (sync)
			SyncPlay();
		else
			NonSyncPlay();
	}
}


bool CPlayVideoTask::SetReadyImage(AVFrame* frm)
{
	Image* img = NULL;
	const int count = m_imgs.size();
	if (count >= IMAGE_MAX_COUNT)
		return false;

	img = new Image({frm->pts,frm->duration});
	m_imgs.push_back(img);

	return true;
}

void CPlayVideoTask::FreeImage(Image** pptr)
{
	Image* img = *pptr;

	delete img;
	img = NULL;
}

CPlayVideoTask::Image* CPlayVideoTask::PopReadyImage()
{
	if (m_imgs.size() == 0)
		return NULL;

	Image* img = NULL;
	auto it = m_imgs.begin(); 

	img = *it;
	*it = NULL;
	m_imgs.erase(it);

	return img;
}

bool CPlayVideoTask::FirstPlay(Image* img)
{
	if (m_Time.pts == 0.0f && \
		m_Time.duration == 0.0f && \
		m_Time.ms == 0 && m_Time.delta == 0)
	{
		RealPlay(img);

		m_Time.pts = img->pts;
		m_Time.duration = img->duration;
		m_Time.ms = m_Access->Now();
		m_Time.delta = 0;

		return true;
	}

	return false;
}

void CPlayVideoTask::RealPlay(Image* img)
{

}


unsigned int  CPlayVideoTask::RemainTime()
{
	const int overtime = m_Time.ms + m_Time.duration - m_Time.delta;
	const unsigned long long tim = m_Access->Now();

	if (overtime >= tim)
		return 0;

	return tim - overtime;
}

void	CPlayVideoTask::UpdateTime(Image* img)
{
	const int overtime = m_Time.ms + m_Time.duration - m_Time.delta;
	const unsigned long long tim = m_Access->Now();
	const int delta = tim - overtime;


	m_Time.pts = img->pts;
	m_Time.duration = img->duration;
	m_Time.ms = tim;

	if (delta >= FORCE_MAX_DELTA)
	{

	}

	m_Time.delta = delta >= FORCE_MAX_DELTA ? 0 : delta;
}
