#include "Media/Task/Decode/DecodeTask.h"

CDecodeTask::CDecodeTask(ITaskAccess* access,const int streamindex):\
						mAccess(access), mStreamIndex(streamindex), \
						 mBuffPkt(NULL),mPkt(NULL),mFrm(NULL)
{
	mBuffPkt = new CProducerConsumer<AVPacket >(MAX_PACKET_COUNT);
}

CDecodeTask::~CDecodeTask()
{

}

void CDecodeTask::RunProcess()
{
	return Loop();
}

void CDecodeTask::Loop()
{
	AVCodecContext* codec = mAccess->CodecContext(mStreamIndex);

	while (1)
	{
		//pop packet
		if (mPkt == NULL)
		{
			mPkt = mBuffPkt->Pop();
			continue;
		}

		//decode packet
		if (mFrm == NULL)
		{
			mFrm = DecodePacket(codec, mPkt);

			FreePacket(&mPkt);
			mPkt = NULL;
			continue;
		}


		//push frame
		mAccess->PushFrame(mFrm, mStreamIndex);
		mFrm = NULL;
	}
}

void CDecodeTask::PushPacket(AVPacket* pkt)
{
	mBuffPkt->Push(pkt);
}