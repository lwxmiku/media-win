#pragma once

#include "Media/Util/MediaUtils.h"
#include "Media/Thread/WorkThread.h"
#include "Media/Task/ITaskAccess.h"
#include "Media/Thread/ProducerConsumer.h"

#define  MAX_PACKET_COUNT 10


class CDecodeTask :public CWorkThread
{
public:
	CDecodeTask(ITaskAccess* access,const int streamindex);
	~CDecodeTask();

protected:
	void RunProcess();
	void Loop();

public:
	void PushPacket(AVPacket* pkt);	//call by demux task

	
private:
	ITaskAccess* mAccess;
	const int mStreamIndex;

	CProducerConsumer<AVPacket>* mBuffPkt;
	AVPacket* mPkt;
	AVFrame* mFrm;

};
