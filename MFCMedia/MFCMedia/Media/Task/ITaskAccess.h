#pragma once

#include "Media/Util/MediaUtils.h"
#include "Media/Util/CvtImg.h"
#include "Media/Util/CvtSnd.h"

class ITaskAccess
{
protected:
	~ITaskAccess() = default;
public:
	ITaskAccess() = default;

public:
	//demux
	virtual AVFormatContext* FormatContext() = 0;
	virtual void PushPakcet(AVPacket* pkt) = 0;

	//decode
	virtual AVCodecContext* CodecContext(const int streamindex) = 0;
	virtual void PushFrame(AVFrame* frm, const int streamIndex) = 0;

	//convert
	virtual  imgcvt::CImageConvert* ImageConvert() = 0;
	virtual  sndcvt::CSoundConvert* SoundConvert() = 0;

	//stream information
	virtual double StreamTimeBase(const int streamindex) = 0;

	//time information
	virtual unsigned long long Now() = 0;

	//show device
	virtual void RendererImage() = 0;
	virtual void PlaybackSound() = 0;
	virtual void ShowText() = 0;

	//synchronization
	virtual bool SynchronizationTime(double* outPts,double* outDuration) = 0;
	virtual bool Synchronization() = 0;
};


struct PlayedTimeClock
{
	double pts;			//unit is second
	double duration;	//unit is second

	unsigned long long ms;		//unit is millisecond
	unsigned long long delta;	//overtime ,unit is millisecond
};



