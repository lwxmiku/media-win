#pragma once

#include "Media/Util/CvtSnd.h"

#include <vector>
#include <algorithm>
using namespace std;

class CAudioSound
{
public:
	CAudioSound(const double timebase, sndcvt::CSoundConvert* cvt);
	~CAudioSound();

	void Push(AVFrame* frm);
	double Duration();
	
	sndcvt::Sound* Convert();
	void  Convert(int64_t* outpts,int64_t* outduration);

private:
	int64_t DurationRaw();
	void  Free();

private:
	const double m_TimeBase;
	sndcvt::CSoundConvert* m_Cvt;
	vector<AVFrame*> m_Snds;
};
