#include "AudioSound.h"

CAudioSound::CAudioSound( const double timebase, sndcvt::CSoundConvert* cvt):\
						m_TimeBase(timebase),m_Cvt(cvt)
{

}

CAudioSound::~CAudioSound()
{
	Free();
}

void CAudioSound::Push(AVFrame* frm)
{
	m_Snds.push_back(frm);
}

double CAudioSound::Duration()
{
	return (((double)DurationRaw()) * m_TimeBase);
}

sndcvt::Sound* CAudioSound::Convert()
{
	const int count = m_Snds.size();
	int64_t pts = 0, duration = 0;
	auto it = m_Snds.begin(), bit = m_Snds.begin(), eit = m_Snds.end();
	sndcvt::Sound* snd = NULL;

	if (count <= 0)
		return NULL;

	pts = (*bit)->pts;
	duration = DurationRaw();

	Free();

	return snd;
}

void  CAudioSound::Convert(int64_t* outpts, int64_t* outduration)
{
	const int count = m_Snds.size();
	auto it = m_Snds.begin(), bit = m_Snds.begin(), eit = m_Snds.end();

	if (count <= 0)
	{
		*outpts = 0;
		*outduration = 0;
		return;
	}

	*outpts = (*bit)->pts;
	*outduration = DurationRaw();

}

void  CAudioSound::Free()
{
	auto it = m_Snds.begin(), bit = m_Snds.begin(), eit = m_Snds.end();
	AVFrame* frm = NULL;

	for (it = bit; it != eit; it++)
	{
		frm = *it;
		*it = NULL;
	
		FreeFrame(&frm);
		frm = NULL;
	}

	m_Snds.clear();
}

int64_t CAudioSound::DurationRaw()
{
	auto it = m_Snds.begin(), bit = m_Snds.begin(), eit = m_Snds.end();
	int64_t duration = 0;

	for (it = bit; it != eit; it++)
	{
		duration += (*it)->duration;
	}

	return duration;
}