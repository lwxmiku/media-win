#pragma once

#include "Media/Util/MediaUtils.h"
#include "Media/Util/CvtSnd.h"

#include "Media/Task/BasePlayTask.h"

#include "Media/Task/PlayAudio/AudioSound.h"

#include <vector>
#include <algorithm>
using namespace std;

#define AUDIO_MAX_FRAME_COUNT 40
#define AUDIO_MAX_TIMELENGHT 0.5f 


class CPlayAudioTask :public CWorkThread,public CBasePlayTask
{
public:
	CPlayAudioTask(ITaskAccess* access, const int streamindex);
	~CPlayAudioTask();

	struct Sound
	{
		int64_t pts;
		int64_t duration;
	};

	void AudioTime(double* outpts,double* outduration);

protected:
	void RunProcess();
	void Loop();

	

private:
	bool Packing();
	void ConvertSound();
	void PlayFunction();


	
	void Play4Device();
	bool FirstPlay();
	unsigned int  RemainTime();
	void NormalPlay();

private:
	AVFrame* m_Frm;
	CAudioSound* m_Sample;
	Sound* m_Snd;
};