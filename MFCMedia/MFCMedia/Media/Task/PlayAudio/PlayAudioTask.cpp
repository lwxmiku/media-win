#include "Media/Task/PlayAudio/PlayAudioTask.h"

CPlayAudioTask::CPlayAudioTask(ITaskAccess* access, const int streamindex):\
								CBasePlayTask(access, streamindex, AUDIO_MAX_FRAME_COUNT),\
								m_Frm(NULL),\
								m_Sample(NULL),m_Snd(NULL)
{
	m_Sample = new CAudioSound(m_TimeBase,access->SoundConvert());
}

CPlayAudioTask::~CPlayAudioTask()
{

}


void CPlayAudioTask::RunProcess()
{
	return Loop();
}


void CPlayAudioTask::Loop()
{
	bool pack = false;

	while (1)
	{
		pack = Packing();
		if (pack)
		{
			continue;
		}
		else
		{
			//convert
			ConvertSound();

			PlayFunction();
		}
		
	}
}

bool CPlayAudioTask::Packing()
{
	if (m_Sample->Duration() >= AUDIO_MAX_TIMELENGHT)
		return false;


	//get frame
	if (m_Frm == NULL)
	{
		m_Frm = m_Buff->Pop();
	}

	if (m_Frm->format == -1)
	{
		FreeFrame(&m_Frm);
		m_Frm = NULL;
	}

	m_Sample->Push(m_Frm);
	m_Frm = NULL;

	return true;

}

void CPlayAudioTask::ConvertSound()
{
	int64_t pts = 0, duration = 0;
	Sound* snd = new Sound({0,0});

	m_Sample->Convert(&pts, &duration);

	snd->pts = pts;
	snd->duration = duration;

	if (m_Snd)
	{
		delete m_Snd;
		m_Snd = NULL;
	}

	m_Snd = snd;
	
}

void CPlayAudioTask::PlayFunction()
{
	bool play = false;
	unsigned int  remain = 0;

	while (1)
	{
		play = FirstPlay();
		if (play)
			break;

		remain = RemainTime();
		if (remain == 0)
		{
			NormalPlay();
			break;
		}
		else
		{
			Sleep(remain / 2);
		}
	}
}

bool CPlayAudioTask::FirstPlay()
{
	if (m_Time.pts == 0.0f && \
		m_Time.duration == 0.0f && \
		m_Time.ms == 0 && \
		m_Time.delta == 0)
	{
		Play4Device();

		//update clock
		m_Time.pts = m_Snd->pts * m_TimeBase;
		m_Time.duration = m_Snd->duration * m_TimeBase;
		m_Time.ms = m_Access->Now();
		m_Time.delta = 0;

		return true;
	}

	return false;
}

unsigned int  CPlayAudioTask::RemainTime()
{
	const int overtime = m_Time.ms + m_Time.duration - m_Time.delta;
	const unsigned long long tim = m_Access->Now();

	if (overtime >= tim)
		return 0;
	
	return tim - overtime;
}

void CPlayAudioTask::Play4Device()
{

}

void CPlayAudioTask::NormalPlay()
{
	const int overtime = m_Time.ms + m_Time.duration - m_Time.delta;

	Play4Device();

	m_Time.ms = m_Access->Now();
	m_Time.delta = (m_Time.ms - overtime) >= 100 ? 0 : (m_Time.ms - overtime);//force set time

	m_Time.pts = m_Snd->pts;
	m_Time.duration = m_Snd->duration;
}




void CPlayAudioTask::AudioTime(double* outpts, double* outduration)
{
	*outpts = m_Time.pts;
	*outduration = m_Time.duration;
}