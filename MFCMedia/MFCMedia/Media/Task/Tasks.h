#pragma once

#include "Media/Task/Demux/DemuxTask.h"
#include "Media/Task/Decode/DecodeTask.h"
#include "Media/Task/PlayAudio/PlayAudioTask.h"
#include "Media/Task/PlayVideo/PlayVideoTask.h"
#include "Media/Task/PlaySubtitle/PlaySubtitleTask.h"


struct MediaContext
{
	AVFormatContext* fmtCtx;

	int audioStreamIndex;
	AVCodecContext* audioCtx;

	int videoStreamIndex;
	AVCodecContext* videoCtx;

	int subtitleStreamIndex;
	AVCodecContext* subtitleCtx;
};

class IMediaDevice;

class CTasks : public ITaskAccess
{
public:
	CTasks(MediaContext* ctx, IMediaDevice* device);
	~CTasks();

	enum State
	{
		STATE_NONE = -1,
		STATE_PLAY,
		STATE_PAUSE,
		STATE_STOP
	};
	 
private:
	void Init();
	void InitTask();
	void InitConvert();
	void InitImageConvert();
	void InitSoundConvert();

public:
	void Play();
	void Stop();
	void Pause();

private:
	void Start();
	void Resume();

public:
	//demux
	AVFormatContext* FormatContext() ;
	void PushPakcet(AVPacket* pkt) ;

	//decode
	AVCodecContext* CodecContext(const int streamindex) ;
	void PushFrame(AVFrame* frm, const int streamIndex) ;

	//convert
	imgcvt::CImageConvert* ImageConvert() ;
	sndcvt::CSoundConvert* SoundConvert() ;

	//stream information
	double StreamTimeBase(const int streamindex) ;

	//time information
	unsigned long long Now();

	//show device
	void RendererImage() ;
	void PlaybackSound() ;
	void ShowText() ;
	
	//
	bool SynchronizationTime(double* outPts, double* outDuration);
	bool Synchronization() ;


private:
	//reference
	 MediaContext* m_Ctx;
	 IMediaDevice* m_Device;

	 //convert 
	 imgcvt::CImageConvert* m_ImgCvt;
	 sndcvt::CSoundConvert* m_SndCvt;

	 //state manager
	 State			   m_State;

	//demux task
	CDemuxTask*     m_Demux;


	//audio	task
	CDecodeTask*	m_DecodeAudio;
	CPlayAudioTask* m_PlayAudio;

	//video task
	CDecodeTask*		m_DecodeVideo;
	CPlayVideoTask*		m_PlayVideo;

	//subtitle task
	CDecodeTask*	   m_DecodeSubtitle;
	CPlaySubtitleTask* m_PlaySubtitle;
};
