#pragma once

#include "Media/Util/MediaUtils.h"
#include "Media/Thread/WorkThread.h"
#include "Media/Task/ITaskAccess.h"
#include "Media/Thread/ProducerConsumer.h"

#include "Media/Task/BasePlayTask.h"

#define SUBTITLE_MAX_FRAME_COUNT 10


class CPlaySubtitleTask :public CWorkThread ,public CBasePlayTask
{
public:
	CPlaySubtitleTask(ITaskAccess* access, const int streamindex);
	~CPlaySubtitleTask();

protected:
	void RunProcess();
	void Loop();
private:


private:
	AVFrame* m_Frm;

};
