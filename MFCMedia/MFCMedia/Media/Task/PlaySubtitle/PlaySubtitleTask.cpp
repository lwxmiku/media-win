#include "Media/Task/PlaySubtitle/PlaySubtitleTask.h"

CPlaySubtitleTask::CPlaySubtitleTask(ITaskAccess* access, const int streamindex) :\
	CBasePlayTask(access,streamindex, SUBTITLE_MAX_FRAME_COUNT), m_Frm(NULL)
{
	
}

CPlaySubtitleTask::~CPlaySubtitleTask()
{

}


void CPlaySubtitleTask::RunProcess()
{
	return Loop();
}

void CPlaySubtitleTask::Loop()
{
	while (1)
	{
		if (m_Frm == NULL)
		{
			m_Frm = m_Buff->Pop();
			continue;
		}

		if (m_Frm->format == -1)
		{
			FreeFrame(&m_Frm);
			m_Frm = NULL;
			continue;;
		}

		//test thread
		FreeFrame(&m_Frm);
		m_Frm = NULL;
	}
}





