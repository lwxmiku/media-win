#pragma once

/*

#include "Media/MediaUtils.h"

#include "Media/Demux.h"
#include "Media/Decode.h"
#include "Media/PlayAudio.h"
#include "Media/PlayVideo.h"
#include "Media/PlaySubtitle.h"


class IPlayDevice
{
protected:
	IPlayDevice() = default;
	virtual ~IPlayDevice() = default;
public:
	virtual void RendererImage(BitmapRgba* bitmap) = 0;
};


class CMediaPlaying : public IMediaInterface
{
private:
	CMediaPlaying(AVFormatContext* fmtCtx,IPlayDevice* device);
public:
	~CMediaPlaying();
	static CMediaPlaying* Open(const char* path, IPlayDevice* device);


	void Play();
	void Stop();
	void Seek();

public:
	 AVFormatContext*& FormatContext() ;
	 void PushPakcet(AVPacket* pkt) ;

	//decode
	AVCodecContext* CodecContext(const int streamindex);
	void PushFrame(AVFrame* frm, const int streamIndex) ;

	//play
	AVRational TimeBase(const int streamindex);

	int64_t NowTime();

	bool SyncAudio() ;
	void AudioTime(double* outpts, double* outduration) ;

	void RendererImage(AVFrame* raw) ;
	void SpeakSound();
	void ShowText() ;

protected:
	void InitDemux();

	void InitCodecs();
	void InitAudio();
	void InitVideo();
	void InitSubtitle();

private:
	struct Audio
	{
		int				streamIndex;
		AVCodecContext* codec;
		CDecode*        decode;
		CPlayAudio*		play;
	};

	struct Video
	{
		int				streamIndex;
		AVCodecContext* codec;
		CDecode*        decode;
		CPlayVideo*		play;
	};

	struct Subtitle
	{
		int				streamIndex;
		AVCodecContext* codec;
		CDecode*        decode;
		CPlaySubtitle*		play;
	};

	IPlayDevice* m_refDevice;

	AVFormatContext* m_fmtCtx;
	
	CDemux* m_demux;
	Audio m_audio;
	Video m_video;
	Subtitle m_subtitle;

	//
	SwsContext* m_cvtImg;
	

};


*/