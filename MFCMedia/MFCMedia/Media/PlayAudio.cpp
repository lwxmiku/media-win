#include "Media/PlayAudio.h"

/*

CPlayAudio::CPlayAudio(IMediaInterface* media, const int streamindex, const int maxCount):\
	m_refMedia(media),m_streamindex(streamindex),m_maxCount(maxCount),\
	m_frms(NULL),\
	m_ctrltime({NULL}),m_lastest(NULL)
{
	m_frms = new CProducerConsumer<AVFrame>(maxCount);
}

CPlayAudio::~CPlayAudio()
{

}

void CPlayAudio::PushFrame(AVFrame* frm)
{
	m_frms->Push(frm);
}

void CPlayAudio::RunProcess()
{
	return Loop();
}

void CPlayAudio::Loop()
{
	while (1)
	{
		if (m_lastest == NULL)
		{
			m_lastest = m_frms->Pop();

			if (m_lastest->format == -1)
			{
				FreeFrame(&m_lastest);
				m_lastest = NULL;
			}

			continue;
		}
			
		PlayFrame(m_lastest);

		FreeFrame(&m_lastest);
		m_lastest = NULL;
	}
}

bool CPlayAudio::FirstPlay(AVFrame* frm)
{
	bool first = false;

	if (m_ctrltime.pts == 0 && \
		m_ctrltime.duration == 0 && \
		m_ctrltime.nowtime == 0 && \
		m_ctrltime.delta == 0)
	{
		m_refMedia->SpeakSound();
		UpdateControlTime(frm->pts,frm->duration, m_refMedia->NowTime(),0);

		first = true;
	}

	return first;
}

void CPlayAudio::UpdateControlTime(int64_t pts, int64_t duration, int64_t nowtime, int delta)
{
	m_ctrltime.pts = pts;
	m_ctrltime.duration = duration;

	m_ctrltime.nowtime = nowtime;
	m_ctrltime.delta = delta;
}

int CPlayAudio::TryPlay(AVFrame* frm)
{
	const int remainms = CheckPlayOver();

	if (remainms == 0)
	{
		NormalPlay(frm);
	}
	

	return remainms;
}

int CPlayAudio::CheckPlayOver()
{
	const double tbms = TimeBase();
	const int64_t nextplaytime = m_ctrltime.nowtime + (m_ctrltime.duration * tbms) - m_ctrltime.delta;
	const int64_t nowtime = m_refMedia->NowTime();
	const bool over = nowtime >= nextplaytime;
	int remaintime = 0;

	if (over == false)
		remaintime = nowtime - (nextplaytime);
	else
		remaintime = 0;

	return remaintime;
}



void CPlayAudio::PlayFrame(AVFrame* frm)
{
	bool first = false;;
	int waitms = 0;

	while (1)
	{
		first = FirstPlay(frm);
		if (first)
			break;

		waitms = TryPlay(frm);
		if (waitms == 0)
			break;
		else
			Sleep(waitms/2);
	}

}


const double CPlayAudio::TimeBase()
{
	static const double tb = av_q2d(m_refMedia->TimeBase(m_streamindex));
	static const double tbms = 1000.0f * tb;

	return tbms;
}

void CPlayAudio::NormalPlay(AVFrame* frm)
{
	const double tbms = TimeBase();
	const int64_t nextplaytime = m_ctrltime.nowtime + (m_ctrltime.duration * tbms) - m_ctrltime.delta;

	//play
	m_refMedia->SpeakSound();
	int64_t nowtime = m_refMedia->NowTime();

	//update
	m_ctrltime.pts = frm->pts;
	m_ctrltime.duration = frm->duration;

	m_ctrltime.nowtime = nowtime;
	m_ctrltime.delta = nowtime - nextplaytime;

	//check delta
	m_ctrltime.delta = m_ctrltime.delta >= 300 ? 0 : m_ctrltime.delta;
}


void CPlayAudio::AudioTime(double* outpts, double* outduration)
{
	*outpts = ((double)m_ctrltime.pts)  * TimeBase();
	*outduration = ((double)m_ctrltime.duration) * TimeBase();
}

*/