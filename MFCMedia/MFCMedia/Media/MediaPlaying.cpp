#include "Media/MediaPlaying.h"

/*

CMediaPlaying::CMediaPlaying(AVFormatContext* fmtCtx, IPlayDevice* device):\
	m_fmtCtx(fmtCtx),m_refDevice(device),\
	m_demux(NULL),
	m_audio({-1, NULL }), m_video({-1, NULL }), m_subtitle({-1,NULL}),\
	m_cvtImg(NULL)
{
	InitDemux();

	InitCodecs();
}

CMediaPlaying::~CMediaPlaying()
{

}

void CMediaPlaying::InitDemux()
{
	m_demux = new CDemux(this);
}

void CMediaPlaying::InitCodecs()
{
	InitAudio();
	InitVideo();
	InitSubtitle();
}

void CMediaPlaying::InitAudio()
{
	int index = FindAudioStreamIndex(m_fmtCtx);
	if (index < 0)
		return;

	m_audio.streamIndex = index;
	m_audio.codec = CreateDecoder(m_fmtCtx->streams[index]);
	m_audio.decode = new CDecode(this,index,10);
	m_audio.play = new CPlayAudio(this,index,10);
	
}

void CMediaPlaying::InitVideo()
{
	int index = FindVideoStreamIndex(m_fmtCtx);
	if (index < 0)
		return;

	m_video.streamIndex = index;
	m_video.codec = CreateDecoder(m_fmtCtx->streams[index]);
	m_video.decode = new CDecode(this,index,10);
	m_video.play = new CPlayVideo(this,index,10);
}

void CMediaPlaying::InitSubtitle()
{
	int index = FindSubtitleStreamIndex(m_fmtCtx);
	if (index < 0)
		return;

	m_subtitle.streamIndex = index;
	m_subtitle.codec = CreateDecoder(m_fmtCtx->streams[index]);
	m_subtitle.decode = new CDecode(this,index,10);
	m_subtitle.play = new CPlaySubtitle(this,index,10);

}

CMediaPlaying* CMediaPlaying::Open(const char* path, IPlayDevice* device)
{
	AVFormatContext* fmt = NULL;
	CMediaPlaying*  media = NULL;

	fmt = OpenFormatContext(path);
	if (fmt == NULL)
		goto fail;

	media = new CMediaPlaying(fmt,device);

	return media;

fail:
	if (fmt) CloseFormatContext(&fmt);
	return NULL;
}


AVFormatContext*& CMediaPlaying::FormatContext()
{
	return m_fmtCtx;
}

void CMediaPlaying::PushPakcet(AVPacket* pkt)
{
	if (pkt->stream_index == m_audio.streamIndex)
	{
		m_audio.decode->PushPakcet(pkt);
	}
	else if (pkt->stream_index == m_video.streamIndex)
	{
		m_video.decode->PushPakcet(pkt);
	}
	else if (pkt->stream_index == m_subtitle.streamIndex)
	{
		m_subtitle.decode->PushPakcet(pkt);
	}
	else
	{
		FreePacket(&pkt);
		pkt = NULL;
	}
}

//decode
AVCodecContext* CMediaPlaying::CodecContext(const int streamindex)
{
	if (streamindex == m_audio.streamIndex)
	{
		return m_audio.codec;
	}
	else if (streamindex == m_video.streamIndex)
	{
		return m_video.codec;
	}
	else if (streamindex == m_subtitle.streamIndex)
	{
		return m_subtitle.codec;
	}
	else
	{
		return NULL;
	}
}

void CMediaPlaying::PushFrame(AVFrame* frm, const int streamIndex)
{
	if (streamIndex == m_audio.streamIndex)
	{
		m_audio.play->PushFrame(frm);
	}
	else if (streamIndex == m_video.streamIndex)
	{
		m_video.play->PushFrame(frm);
	}
	else if (streamIndex == m_subtitle.streamIndex)
	{
		m_subtitle.play->PushFrame(frm);
	}
	else
		return;
}

//play
AVRational CMediaPlaying::TimeBase(const int streamindex)
{
	return m_fmtCtx->streams[streamindex]->time_base;
}

int64_t CMediaPlaying::NowTime()
{
	unsigned long long timems = TimeMillisecond();
	return timems;
}

bool CMediaPlaying::SyncAudio()
{
	bool existaudio = m_audio.streamIndex != -1;

	return existaudio;
}

void CMediaPlaying::AudioTime(double* outpts, double* outduration)
{
	if (SyncAudio())
		m_audio.play->AudioTime(outpts, outduration);

}

void CMediaPlaying::RendererImage(AVFrame* raw)
{
	BitmapRgba* bitmap = NULL;

	if (m_cvtImg == NULL)
	{
		AVPixelFormat fmt = (AVPixelFormat)raw->format;
		m_cvtImg = CreateSws(raw->width, raw->height, fmt);
	}


	bitmap = BitmapConvert(m_cvtImg,raw);

	m_refDevice->RendererImage(bitmap);

	BitmapFree(&bitmap);


	return;
}

void CMediaPlaying::SpeakSound()
{

}

void CMediaPlaying::ShowText()
{

}

void CMediaPlaying::Play()
{
	if (m_demux)
		m_demux->Start();

	
	if (m_audio.streamIndex != -1)
	{
		m_audio.decode->Start();
		m_audio.play->Start();
	}

	if (m_video.streamIndex != -1)
	{
		m_video.decode->Start();
		m_video.play->Start();
	}
	

	if (m_subtitle.streamIndex != -1)
	{
		m_subtitle.decode->Start();
		m_subtitle.play->Start();
	}
}

void CMediaPlaying::Stop()
{

}

void CMediaPlaying::Seek()
{

}


*/