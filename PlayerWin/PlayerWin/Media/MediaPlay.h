#pragma once

#include <vector>
#include <algorithm>
using namespace  std;

#include "Media/common.h"

#include "Media/DemuxThread.h"
#include "Media/DecodeThread.h"
#include "Media/AudioPlayThread.h"
#include "Media/VideoPlayThread.h"
#include "Media/SubtitlePlayThread.h"

#include "Media/Clock.h"

enum  PlaySpeedEnum;
class IPlayDriver;
class CMediaPlay;


class CMediaPlay : IMediaInterface
{
	struct Demux
	{
		AVFormatContext* fmtCtx;
		CDemuxThread*    demux;
	};

	struct Track
	{
		int streamindex;
		AVCodecContext* codec;
		CDecodeThread*  decode;
	};

	struct AudioTrack
	{
		Track track;
		CAudioPlayThread* play;
	};

	struct VideoTrack
	{
		Track track;
		CVideoPlayThread* play;
	};

	struct SubtitleTrack
	{
		Track track;
		CSubtitlePlayThread* play;
	};

protected:
	CMediaPlay();
public:
	~CMediaPlay();
	static CMediaPlay* OpenMediaFile(const char* path);

	int Play();
	int Pause();
	int Seek(int64_t pos);
	int SetSpeed(PlaySpeedEnum speed);

	int64_t Duration() const; //unit is milliseconds
	int64_t Pts() const; //unit is milliseconds

protected:
	void InitFormatContext(AVFormatContext* fmt);
	void InitCodecContext();
	
	void StartThread();


	void InitAudioTrack(const int streamIndex);
	void InitVideoTrack(const int streamIndex);
	void InitSubtitleTrack(const int streamIndex);

protected:
	//demux thread
	 AVFormatContext*& FormatContex() ;
	 void PushDemuxPacket(AVPacket* pkt) ;

	//decode thread
	 AVCodecContext*& CodecContext(const int streamindex) ;
	 void  PushDecodeFrame(const int streamindex, AVFrame* frm) ;

	//play thread
	AVRational StreamTimeBase(const int streamindex) ;

	void AudioParamter();
	void VideoParamter(int* outWidth, int* outHeight, AVPixelFormat* outFmt);
	void SubtitleParamter();

	void SynchronousTime(bool *outExist, int64_t* outPts, int64_t* outDuration);
	void RealTime(int64_t* outClockTime);

	void PlaySound(SoundFmt* snd);
	void RendererImage(RgbFmt* rgb);
	void RendererText(TextFmt* txt);

private:
	PlaySpeedEnum m_speed;
	
	Demux      m_demux;
	AudioTrack m_audio;
	VideoTrack m_video;
	SubtitleTrack m_subtitle;

	unsigned long long m_clocktime;
};


enum PlaySpeedEnum
{
	SPEED_1_0x,
	SPEED_1_5x, SPEED_2_0x, SPEED_2_5x, SPEED_3_0x,
	SPEED_0_25x, SPEED_0_50x, SPEED_0_75x,
};


#define  MAX_PACKET_COUNT 20
#define  MAX_AUDIO_FRAME_COUNT 50
#define  MAX_VIDEO_FRAME_COUNT 10
#define  MAX_SUBTITLE_FRAME_COUNT 50




