#include "Media/DecodeThread.h"

CDecodeThread::CDecodeThread(IMediaInterface* media, const int streamindex, const int maxcount):\
			m_index(streamindex),m_media(media), m_maxCount(maxcount),\
			m_pkt(NULL),m_frm(NULL)
{
	m_prod = new CProdCondProblem<AVPacket>(m_maxCount);
}

CDecodeThread::~CDecodeThread()
{

}

void CDecodeThread::StartProcess()
{
	Start();
}

void CDecodeThread::RunProcess()
{
	int ret = -1;
	AVCodecContext*& codec = m_media->CodecContext(m_index);

	while (1)
	{
		if (m_pkt == NULL)
		{
			m_pkt = PopPacket();
			continue;
		}

		if (m_frm == NULL)
		{
			m_frm = av_frame_alloc();
			continue;
		}

		ret = DecodePacket(codec, m_pkt, &m_frm);

		//free packet
		av_packet_free(&m_pkt);
		m_pkt = NULL;

		//push frame
		if (AVERROR(EAGAIN) == ret)
		{
			av_frame_free(&m_frm);
			m_frm = NULL;
		}
		else if (AVERROR_EOF == ret)
		{
			av_frame_free(&m_frm);
			m_frm = NULL;
		}
		else if (ret < 0)
		{
			av_frame_free(&m_frm);
			m_frm = NULL;
			break;
		}
		else
		{
			m_media->PushDecodeFrame(m_index,m_frm);
			m_frm = NULL;
		}

		
	}
}

void CDecodeThread::PushPacket(AVPacket* pkt)
{
	m_prod->Push(pkt);
}

AVPacket* CDecodeThread::PopPacket()
{
	return m_prod->Pop();
}