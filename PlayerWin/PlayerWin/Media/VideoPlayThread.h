#pragma once

#include "Media/common.h"
#include "Thread/WorkThread.h"
#include "Thread/ProdCondProblem.h"
#include "Media/MediaInterface.h"


class CVideoPlayThread : public CWorkThread
{
public:
	CVideoPlayThread(IMediaInterface* media, const int streamindex, const int maxcount);
	~CVideoPlayThread();

	void StartProcess();

	void PushImageFrame(AVFrame* frm);	//call by other thread
protected:
	void RunProcess();
private:
	void InitSws();

	double UnitMs();

	AVFrame* PopSoundFrame();

	RgbFmt* Convert(AVFrame* img);

	void Play(RgbFmt* rgb);
	bool TryPlay(RgbFmt* rgb);

	void ForcePlay(RgbFmt* rgb);
	void NormalPlay(RgbFmt* rgb);

	void TimeFrontBack(RgbFmt* rgb,int64_t syncPts, int64_t syncDuration,bool* outFront,bool* outBack, int64_t* outBacktime);

	void DiscardFrame(RgbFmt* rgb);
	
	bool CheckForcePlay(RgbFmt* rgb);
	bool CheckCurrentTimeOver();


	bool PlaySynchronousMySelf(RgbFmt* rgb);
	bool PlaySynchronous(RgbFmt* rgb, int64_t syncpts, int64_t syncduration);
private:
	IMediaInterface* m_media;
	const int m_index;
	const int m_max;

	CProdCondProblem<AVFrame>* m_prod;

	AVFrame* m_frm;
	RgbFmt*  m_rgb;

	TimeInformation m_time;

	SwsContext*  m_sws;
	int m_width, m_heigt;
	AVPixelFormat m_pixfmt;
};
