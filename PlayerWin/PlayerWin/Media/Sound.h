#pragma once

#include "Media/common.h"

struct SoundFmt
{
	int64_t		pts;
	int64_t		duration;

	uint8_t* data;
	int      dataSize;
};

static SoundFmt* SoundFmtNew()
{
	SoundFmt* snd = new SoundFmt;

	snd->pts = 0;
	snd->duration = 0;

	snd->data = NULL;
	snd->dataSize = 0;

	return snd;
}

static void SoundFmtFree(SoundFmt** pptr)
{
	SoundFmt* snd = *pptr;

	if (snd->data)
		delete[] snd->data;

	delete snd;
	snd = NULL;

	*pptr = NULL;
}




static SoundFmt* SoundConvert( AVFrame* sample)
{
	SoundFmt* snd = SoundFmtNew();

	snd->pts = sample->pts;
	snd->duration = sample->duration;

	return snd;
}