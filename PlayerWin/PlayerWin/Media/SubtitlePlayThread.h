#pragma once


#include "Media/common.h"
#include "Thread/WorkThread.h"
#include "Thread/ProdCondProblem.h"
#include "Media/MediaInterface.h"


class CSubtitlePlayThread : public CWorkThread
{
public:
	CSubtitlePlayThread(IMediaInterface* media, const int streamindex, const int maxcount);
	~CSubtitlePlayThread();

	void StartProcess();

	void PushTextFrame(AVFrame* frm);	//call by other thread
protected:
	void RunProcess();
private:
	AVFrame* PopTextFrame();
};
