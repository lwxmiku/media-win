#pragma once

#include "Media/common.h"
#include "Thread/WorkThread.h"

#include "Media/MediaInterface.h"

class CDemuxThread : public CWorkThread
{
public:
	CDemuxThread(IMediaInterface* media);
	~CDemuxThread();

	void StartProcess();

protected:
	void RunProcess();
private:
	IMediaInterface*	m_media;

	AVPacket*			m_pkt;
};
