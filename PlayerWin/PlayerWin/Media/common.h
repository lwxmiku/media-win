#pragma once

extern "C"
{
#include "libavformat/avformat.h"
#include "libavformat/avio.h"

#include "libavcodec/avcodec.h"
#include "libavutil/avutil.h"

#include "libswresample/swresample.h"

#include "libavutil/opt.h"
#include "libavutil/samplefmt.h"
#include "libavutil/channel_layout.h"


#include "libswscale/swscale.h"
#include "libavutil/imgutils.h"
#include "libavutil/parseutils.h"

#include "libavutil/time.h"
#include "libavutil/rational.h"



}

#include <cstdio>
#include <cstdlib>
#include <cstdint>

int ReadPacket(AVFormatContext* fmt, AVPacket* pkt);
int DecodePacket(AVCodecContext* codec, AVPacket* pkt, AVFrame** outfrm);
AVFormatContext* OpenFile(const char* path);
AVCodecContext* CreateCodecCtx(AVStream* ref);





