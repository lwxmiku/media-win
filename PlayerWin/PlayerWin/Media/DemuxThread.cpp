#include "Media/DemuxThread.h"


void DemuxCallBackFunc(void* ptr0, void* ptr1);

CDemuxThread::CDemuxThread(IMediaInterface* media):m_media(media),m_pkt(NULL)
{
	
}

CDemuxThread::~CDemuxThread()
{

}

void CDemuxThread::StartProcess()
{
	Start();
}

void CDemuxThread::RunProcess()
{
	int ret = -1;
	AVFormatContext*& fmtctx = m_media->FormatContex();


	while (1)
	{
		if (m_pkt == NULL)
		{
			m_pkt = av_packet_alloc();
		}
		else
		{
			ret = ReadPacket(fmtctx, m_pkt);
			if (0 == ret)
			{
				m_media->PushDemuxPacket(m_pkt);
				m_pkt = NULL;
			}
			else if (AVERROR_EOF == ret)
			{

				//end of file
				Sleep(100);
				continue;
			}
			else
			{
				//error
				break;
			}
		}

	}
}