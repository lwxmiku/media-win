#include "Media/AudioPlayThread.h"

CAudioPlayThread::CAudioPlayThread(IMediaInterface* media, const int streamindex, const int maxcount):\
	m_media(media),m_index(streamindex),m_maxCount(maxcount),\
	m_prod(NULL),m_frm(NULL), m_sound(NULL),\
	m_time({0,0,0,0})
{
	m_prod = new CProdCondProblem<AVFrame>(m_maxCount);
}

CAudioPlayThread::~CAudioPlayThread()
{

}

void CAudioPlayThread::StartProcess()
{
	Start();
}

void CAudioPlayThread::RunProcess()
{
	while (1)
	{
		if (m_frm == NULL)
		{
			m_frm = PopSoundFrame();
			continue;
		}

		if (m_sound == NULL)
		{
			m_sound = Convert(m_frm);

			av_frame_free(&m_frm);
			m_frm = NULL;
			continue;
		}
			
		//save to array
		SaveInArray(m_sound);
	}
}

void CAudioPlayThread::SaveInArray(SoundFmt* snd)
{
	int count = 0;
	
	m_array.push_back(snd);
	count = m_array.size();

	if (count >= 10)
		PlayArraySound();

}

void CAudioPlayThread::ClearArray(vector<SoundFmt*>& snds)
{
	auto it = snds.begin(), bit = snds.begin(), eit = snds.end();
	SoundFmt* fmt = NULL;

	for (it = bit; it != eit; it++)
	{
		fmt = *it;
		*it = NULL;

		SoundFmtFree(&fmt);
		fmt = NULL;
	}

	snds.clear();
}

void CAudioPlayThread::ArrayDurationPts(vector<SoundFmt*>& snds,int64_t* outPts, int64_t* outDuration)
{
	auto it = snds.begin(), bit = snds.begin(), eit = snds.end();
	int64_t pts = (*bit)->pts;
	int64_t duration = 0;


	for (it = bit; it != eit; it++)
	{
		duration += (*it)->duration;
	}

	*outPts = pts;
	*outDuration = duration;

}

void CAudioPlayThread::PlayArraySound()
{
	int64_t pts = 0, duration = 0;
	SoundFmt* snd = SoundFmtNew();

	ArrayDurationPts(m_array, &pts, &duration);
	
	snd->pts = pts;
	snd->duration = duration;

	Play(snd);

	SoundFmtFree(&snd);
}





void CAudioPlayThread::PushSoundFrame(AVFrame* frm)
{
	m_prod->Push(frm);
}

AVFrame* CAudioPlayThread::PopSoundFrame()
{
	return m_prod->Pop();
}

SoundFmt* CAudioPlayThread::Convert(AVFrame* frm)
{
	SoundFmt* snd = SoundFmtNew();
	
	snd->pts = frm->pts ;
	snd->duration = frm->duration ;

	return snd;
}

double CAudioPlayThread::UnitMs()
{
	static const AVRational tb = m_media->StreamTimeBase(m_index);
	static const double ms = 1000.0f;
	static const double dtb = av_q2d(tb);
	static const double unit = dtb * ms;

	return unit;
}

void CAudioPlayThread::ForcePlay(SoundFmt* snd)
{
	int64_t clocktime = 0;

	m_media->PlaySound(snd);

	m_media->RealTime(&clocktime);

	m_time.overtime = 0;
	m_time.clocktime = clocktime;
	m_time.pts = snd->pts * UnitMs();
	m_time.duration = snd->duration *UnitMs();
}

void CAudioPlayThread::NormalPlay(SoundFmt* snd)
{
	int64_t clocktime = 0;
	const int64_t nexttime = (m_time.clocktime + m_time.duration - m_time.overtime);
	int64_t overtime = 0;

	m_media->PlaySound(snd);

	m_media->RealTime(&clocktime);

	overtime = clocktime - nexttime;
	overtime = overtime >= 100 ? 0 : overtime;
	
	m_time.overtime = overtime;
	m_time.clocktime = clocktime;
	m_time.pts = snd->pts * UnitMs();
	m_time.duration = snd->duration * UnitMs();
}

bool CAudioPlayThread::CheckForcePlay(SoundFmt* snd)
{
	int64_t clocktime = 0;
	const int64_t nexttime = (m_time.clocktime + m_time.duration - m_time.overtime);

	m_media->RealTime(&clocktime);

	return ((clocktime - nexttime) >= snd->duration);
}

bool CAudioPlayThread::CheckCurrentTimeOver()
{
	int64_t clocktime = 0;
	const int64_t nexttime = (m_time.clocktime + m_time.duration - m_time.overtime);

	m_media->RealTime(&clocktime);

	return ((clocktime - nexttime) >= 0);
}

bool CAudioPlayThread::TryPlay(SoundFmt* snd)
{
	bool force = false,overtime = false;
	bool isplay = false;

	force = CheckForcePlay(snd);
	if (force)
	{
		ForcePlay(snd);
		isplay = true;
	}
	else
	{
		overtime = CheckCurrentTimeOver();
		if (overtime)
		{
			NormalPlay(snd);
			isplay = true;
		}
		else
		{
			isplay = false;
		}
	}


	return isplay;
}


void CAudioPlayThread::Play(SoundFmt* snd)
{
	bool isplay = false;

	while (1)
	{
		if (m_time.clocktime == 0 && m_time.overtime == 0 &&\
			m_time.pts == 0.0f && m_time.duration == 0.0f)
		{
			ForcePlay(snd);
			break;
		}
		else
		{
			isplay = TryPlay(snd);
			if (isplay)
				break;
			else
				Sleep(1);
		}
	}
}

int64_t CAudioPlayThread::Pts() const
{
	return m_time.pts;
}

int64_t CAudioPlayThread::Duration() const
{
	return m_time.duration;
}

