#pragma once

#include "Media/common.h"

#include "Media/Image.h"
#include "Media/Sound.h"
#include "Media/Text.h"


class IMediaInterface
{
public:
	IMediaInterface() = default;
	~IMediaInterface() = default;

public:
	//demux thread
	virtual AVFormatContext*& FormatContex() = 0;
	virtual void PushDemuxPacket(AVPacket* pkt) = 0;

	//decode thread
	virtual AVCodecContext*& CodecContext(const int streamindex) = 0;
	virtual void  PushDecodeFrame(const int streamindex,AVFrame* frm) = 0;

	//play thread
	virtual AVRational StreamTimeBase(const int streamindex) = 0;

	virtual void AudioParamter() = 0;
	virtual void VideoParamter(int* outWidth,int* outHeight,AVPixelFormat* outFmt) = 0;
	virtual void SubtitleParamter() = 0;

	virtual void SynchronousTime(bool *outExist , int64_t* outPts, int64_t* outDuration) = 0;
	virtual void RealTime(int64_t* outClockTime) = 0;

	virtual void PlaySound(SoundFmt* snd) = 0;
	virtual void RendererImage(RgbFmt* rgb) = 0;
	virtual void RendererText(TextFmt* txt) = 0;

};


struct TimeInformation
{
	int64_t clocktime;
	int64_t overtime;

	int64_t pts;
	int64_t duration;
};
