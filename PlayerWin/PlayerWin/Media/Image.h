#pragma once

#include "Media/common.h"

struct RgbFmt
{
	AVPixelFormat fmt;

	int width, height;

	int64_t pts;			//the unit is ms
	int64_t duration;	//the unit is ms

	//only rgb data
	uint8_t* data;
	int      dataSize;
};

static RgbFmt* RgbFmtNew()
{
	RgbFmt* rgb = new RgbFmt;

	rgb->fmt = AV_PIX_FMT_NONE;

	rgb->width = -1;
	rgb->height = -1;

	rgb->pts = 0.0f;
	rgb->duration = 0.0f;

	rgb->data = NULL;
	rgb->dataSize = 0;

	return rgb;

}

static void RgbFmtFree(RgbFmt** pptr)
{
	RgbFmt* rgb = *pptr;

	if (rgb->data)
		delete[] rgb->data;
	rgb->data = NULL;

	delete rgb;
	rgb = NULL;

	*pptr = NULL;
}

static void Roate180(unsigned char *pData, int image_width, int image_height, int bpp)
{
	int index = bpp / 8;
	for (int h = 0; h < image_height / 2; h++)
	{
		for (int w = 0; w < image_width; w++)
		{
			const int iCoordM = index*(h*image_width + w);
			const int iCoordN = index*((image_height - h - 1)*image_width + w);
			unsigned char Tmp = pData[iCoordM];
			pData[iCoordM] = pData[iCoordN];
			pData[iCoordN] = Tmp;
			Tmp = pData[iCoordM + 1];
			pData[iCoordM + 1] = pData[iCoordN + 1];
			pData[iCoordN + 1] = Tmp;
			Tmp = pData[iCoordM + 2];
			pData[iCoordM + 2] = pData[iCoordN + 2];
			pData[iCoordN + 2] = Tmp;
		}
	}
}



static RgbFmt* ImageConvert(AVFrame* img,\
	SwsContext* ctx,int dstWidth,int dstHeight,AVPixelFormat dstFmt)
{
	RgbFmt* rgb = RgbFmtNew();
	int ret = NULL;
	uint8_t* data[4] = {NULL};
	int      linesize[4] = {NULL};

	rgb->pts = img->pts;
	rgb->duration = img->duration;

	rgb->height = dstWidth;
	rgb->width = dstHeight;
	rgb->fmt = dstFmt;

	rgb->dataSize = av_image_get_buffer_size(rgb->fmt, rgb->width, rgb->height, 1);
	rgb->data = new uint8_t[rgb->dataSize];
	memset(rgb->data,0, rgb->dataSize);

	//alloc target format data
	ret = av_image_alloc(data, linesize, rgb->width, rgb->height, rgb->fmt, 1);

	ret = sws_scale(ctx, img->data, img->linesize, 0, rgb->height, data, linesize);
	if (ret > 0)
	{
		//copy target format data into rgb
		memcpy(rgb->data, data[0], rgb->dataSize);
	}
	else
	{
		RgbFmtFree(&rgb);
	}

	av_freep(&data[0]);

	return rgb;
}



