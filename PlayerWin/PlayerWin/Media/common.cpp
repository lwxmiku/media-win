#include "Media/common.h"

int ReadPacket(AVFormatContext* fmt, AVPacket* pkt)
{

	int ret = -1;

	ret = av_read_frame(fmt, pkt);
	return ret;
}

int DecodePacket(AVCodecContext* codec, AVPacket* pkt, AVFrame** outfrm)
{
	int ret = -1;

	ret = avcodec_send_packet(codec, pkt);
	if (ret < 0)
	{
		//printdebug("err", __FILE__, __LINE__);
		return ret;
	}

	ret = avcodec_receive_frame(codec, *outfrm);

	return ret;
}

AVFormatContext* OpenFile(const char* path)
{
	int ret = -1;
	AVFormatContext* fmtctx = NULL;

	ret = avformat_open_input(&fmtctx, path, NULL, NULL);
	if (ret < 0)
	{
		//printdebug("err", __FILE__, __LINE__);
		goto  fail;
	}

	ret = avformat_find_stream_info(fmtctx, NULL);
	if (ret < 0)
	{
		//printdebug("err", __FILE__, __LINE__);
		goto  fail;
	}

	return fmtctx;

fail:
	if (fmtctx) avformat_free_context(fmtctx);
	fmtctx = NULL;
	return NULL;
}

AVCodecContext* CreateCodecCtx(AVStream* ref)
{
	const AVCodec* decoder = avcodec_find_decoder(ref->codecpar->codec_id);
	AVCodecContext* codectx = NULL;
	int ret = -1;

	if (!decoder)
	{
		//printdebug("err", __FILE__, __LINE__);
		goto fail;
	}


	codectx = avcodec_alloc_context3(decoder);
	if (!codectx)
	{
		//printdebug("err", __FILE__, __LINE__);
		goto fail;
	}


	ret = avcodec_parameters_to_context(codectx, ref->codecpar);
	if (ret < 0)
	{
		//printdebug("err", __FILE__, __LINE__);
		goto fail;
	}

	ret = avcodec_open2(codectx, decoder, NULL);
	if (ret < 0)
	{
		//printdebug("err", __FILE__, __LINE__);
		goto fail;
	}



	return codectx;

fail:
	if (codectx) avcodec_free_context(&codectx);
	codectx = NULL;
	return NULL;
}