#include "Media/MediaPlay.h"

CMediaPlay::CMediaPlay():\
	m_speed(SPEED_1_0x),\
	m_demux({NULL,NULL}),\
	m_audio({ { -1,NULL,NULL },NULL }),\
	m_video({ { -1,NULL,NULL },NULL }),\
	m_subtitle({ { -1,NULL,NULL },NULL }),\
	m_clocktime(0)
{

}

CMediaPlay::~CMediaPlay()
{

}

CMediaPlay* CMediaPlay::OpenMediaFile(const char* path)
{
	AVFormatContext* fmt = OpenFile(path);
	CMediaPlay* media = NULL;

	if (fmt)
	{
		media = new CMediaPlay();

		media->InitFormatContext(fmt);
		media->InitCodecContext();
		

		
	}

	return media;
}

void CMediaPlay::InitFormatContext(AVFormatContext* fmt)
{
	m_demux.fmtCtx = fmt;
	m_demux.demux = new CDemuxThread(this);
}

void CMediaPlay::InitCodecContext()
{
	int ret = -1;

	ret = av_find_best_stream(m_demux.fmtCtx, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);
	if (ret >= 0)
	{
		InitAudioTrack(ret);
	}

	ret = av_find_best_stream(m_demux.fmtCtx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
	if (ret >= 0)
	{
		InitVideoTrack(ret);
	}

	ret = av_find_best_stream(m_demux.fmtCtx, AVMEDIA_TYPE_SUBTITLE, -1, -1, NULL, 0);
	if (ret >= 0)
	{
		InitSubtitleTrack(ret);
	}
}



void CMediaPlay::StartThread()
{
	m_demux.demux->StartProcess();

	if (m_audio.track.streamindex != -1)
	{
		m_audio.track.decode->StartProcess();
		m_audio.play->StartProcess();
	}

	if (m_video.track.streamindex != -1)
	{
		m_video.track.decode->StartProcess();
		m_video.play->StartProcess();
	}

	if (m_subtitle.track.streamindex != -1)
	{
		m_subtitle.track.decode->StartProcess();
		m_subtitle.play->StartProcess();
	}
}

void CMediaPlay::InitAudioTrack(const int streamIndex)
{
	AVCodecContext* codec = CreateCodecCtx(m_demux.fmtCtx->streams[streamIndex]);

	m_audio.track.streamindex = streamIndex;
	m_audio.track.codec = codec;
	m_audio.track.decode = new CDecodeThread(this, m_audio.track.streamindex, MAX_PACKET_COUNT);

	m_audio.play = new CAudioPlayThread(this, m_audio.track.streamindex,MAX_AUDIO_FRAME_COUNT);
}

void CMediaPlay::InitVideoTrack(const int streamIndex)
{
	AVCodecContext* codec = CreateCodecCtx(m_demux.fmtCtx->streams[streamIndex]);

	m_video.track.streamindex = streamIndex;
	m_video.track.codec = codec;
	m_video.track.decode = new CDecodeThread(this, m_video.track.streamindex, MAX_PACKET_COUNT);

	m_video.play = new CVideoPlayThread(this, m_video.track.streamindex,MAX_VIDEO_FRAME_COUNT);
}

void CMediaPlay::InitSubtitleTrack(const int streamIndex)
{
	AVCodecContext* codec = CreateCodecCtx(m_demux.fmtCtx->streams[streamIndex]);

	m_subtitle.track.streamindex = streamIndex;
	m_subtitle.track.codec = codec;
	m_subtitle.track.decode = new CDecodeThread(this, m_subtitle.track.streamindex, MAX_PACKET_COUNT);

	m_subtitle.play = new CSubtitlePlayThread(this, m_subtitle.track.streamindex,MAX_SUBTITLE_FRAME_COUNT);
}

int CMediaPlay::Play()
{
	StartThread();
	return 0;
}

int CMediaPlay::Pause()
{
	return 0;
}

int CMediaPlay::Seek(int64_t pos)
{
	return 0;
}

int CMediaPlay::SetSpeed(PlaySpeedEnum speed)
{
	return 0;
}

int64_t CMediaPlay::Duration() const 
{
	return 0;
}

int64_t CMediaPlay::Pts() const 
{
	return 0;
}


AVFormatContext*& CMediaPlay::FormatContex()
{
	return m_demux.fmtCtx;
}

void CMediaPlay::PushDemuxPacket(AVPacket* pkt)
{
	const bool aExist = m_audio.track.streamindex != -1, \
		vExist = m_video.track.streamindex != -1, \
		sExist = m_subtitle.track.streamindex != -1;

	CDecodeThread* ref = NULL;

	if (aExist && m_audio.track.streamindex == pkt->stream_index)
	{
		ref = m_audio.track.decode;
	}
	else if (vExist && m_video.track.streamindex == pkt->stream_index)
	{
		ref = m_video.track.decode;
	}
	else if (sExist &&  m_subtitle.track.streamindex == pkt->stream_index)
	{
		ref = m_subtitle.track.decode;
	}

	if (ref)
		ref->PushPacket(pkt);
	else
		_ASSERT(false);



}

//decode thread
AVCodecContext*& CMediaPlay::CodecContext(const int streamindex)
{
	const bool aExist = m_audio.track.streamindex != -1, \
		vExist = m_video.track.streamindex != -1, \
		sExist = m_subtitle.track.streamindex != -1;
	AVCodecContext* nullcode = NULL;

	if (aExist && m_audio.track.streamindex == streamindex)
	{
		return m_audio.track.codec;
	}
	else if (vExist && m_video.track.streamindex == streamindex)
	{
		return m_video.track.codec;
	}
	else if (sExist &&  m_subtitle.track.streamindex == streamindex)
	{
		return m_subtitle.track.codec;
	}

	return nullcode;
}

void  CMediaPlay::PushDecodeFrame(const int streamindex, AVFrame* frm)
{
	const bool aExist = m_audio.track.streamindex != -1, \
		vExist = m_video.track.streamindex != -1, \
		sExist = m_subtitle.track.streamindex != -1;

	if (aExist && m_audio.track.streamindex == streamindex)
	{
		m_audio.play->PushSoundFrame(frm);
	}
	else if (vExist && m_video.track.streamindex == streamindex)
	{
		m_video.play->PushImageFrame(frm);
	}
	else if (sExist &&  m_subtitle.track.streamindex == streamindex)
	{
		m_subtitle.play->PushTextFrame(frm);
	}
}

//play thread

AVRational CMediaPlay::StreamTimeBase(const int streamindex)
{
	return m_demux.fmtCtx->streams[streamindex]->time_base;
}

void CMediaPlay::AudioParamter()
{

}

void CMediaPlay::VideoParamter(int* outWidth, int* outHeight, AVPixelFormat* outFmt)
{
	*outWidth = 1920;
	*outHeight = 1080;
	*outFmt = AVPixelFormat::AV_PIX_FMT_RGB24;
}

void CMediaPlay::SubtitleParamter()
{

}


void CMediaPlay::RealTime(int64_t* outClockTime)
{
	*outClockTime = TimeMillisecond();
}

void CMediaPlay::SynchronousTime(bool *outExist, int64_t* outPts, int64_t* outDuration)
{
	const bool aExist = m_audio.track.streamindex != -1;

	if (aExist)
	{
		*outPts = m_audio.play->Pts();
		*outDuration = m_audio.play->Duration();
	}
	else
	{
		*outPts = 0;
		*outDuration = 0;
	}

	*outExist = aExist;
}

void CMediaPlay::PlaySound(SoundFmt* snd)
{

}

void CMediaPlay::RendererImage(RgbFmt* rgb)
{

}

void CMediaPlay::RendererText(TextFmt* txt)
{

}