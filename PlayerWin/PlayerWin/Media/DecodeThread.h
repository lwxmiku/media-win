#pragma once

#include "Media/common.h"
#include "Thread/WorkThread.h"
#include "Thread/ProdCondProblem.h"
#include "Media/MediaInterface.h"

class CDecodeThread : public CWorkThread
{
public:
	CDecodeThread(IMediaInterface* media,const int streamindex,const int maxcount);
	~CDecodeThread();

	void StartProcess();

	void PushPacket(AVPacket* pkt);	//call by other thread

protected:
	void RunProcess();

private:
	AVPacket* PopPacket();

private:
	IMediaInterface* m_media;
	const int m_index;
	const int m_maxCount;

	CProdCondProblem<AVPacket>* m_prod;

	AVPacket* m_pkt;
	AVFrame*  m_frm;
};



