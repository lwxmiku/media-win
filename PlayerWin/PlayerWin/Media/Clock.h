#pragma once


#include "Thread/WorkThread.h"
#include "Media/common.h"


#include <stdio.h>
#include <time.h>
#include <Windows.h>

static unsigned long long TimeMillisecond()
{
	SYSTEMTIME t1;
	struct tm temp;
	unsigned long long ms = 0;

	GetSystemTime(&t1);

	temp.tm_year = t1.wYear - 1900;
	temp.tm_mon = t1.wMonth - 1;
	temp.tm_mday = t1.wDay;
	temp.tm_hour = t1.wHour;
	temp.tm_min = t1.wMinute;
	temp.tm_sec = t1.wSecond;
	temp.tm_isdst = -1;

	time_t timestamp = mktime(&temp);
	
	ms = (unsigned long long) (timestamp * 1000);
	ms += t1.wMilliseconds;

	return ms;
}


