#include "Media/VideoPlayThread.h"

CVideoPlayThread::CVideoPlayThread(IMediaInterface* media, const int streamindex, const int maxcount):\
	m_media(media),m_index(streamindex),m_max(maxcount),\
	m_prod(NULL), m_frm(NULL), m_rgb(NULL),\
	m_time({0,0,0,0}),\
	m_sws(NULL),m_width(0),m_heigt(0),m_pixfmt(AV_PIX_FMT_NONE)
{
	m_prod = new CProdCondProblem<AVFrame>(m_max);

	InitSws();
	
}
CVideoPlayThread::~CVideoPlayThread()
{

}

void CVideoPlayThread::InitSws()
{
	AVCodecContext* ref = m_media->CodecContext(m_index);
	

	m_media->VideoParamter(&m_width, &m_heigt, &m_pixfmt);


	m_sws = sws_getContext(ref->width, ref->height, ref->pix_fmt, \
		m_width, m_heigt, m_pixfmt, \
		SWS_BICUBIC, NULL, NULL, NULL);

	

	if (m_sws == NULL)
		_ASSERT(false);
}

void CVideoPlayThread::StartProcess()
{
	Start();
}

void CVideoPlayThread::PushImageFrame(AVFrame* frm)
{
	m_prod->Push(frm);
}

void CVideoPlayThread::RunProcess()
{
	while (1)
	{
		if (m_frm == NULL)
		{
			m_frm = PopSoundFrame();
			continue;
		}

		if (m_rgb == NULL)
		{
			m_rgb = Convert(m_frm);
			m_frm = NULL;
			continue;
		}


		Play(m_rgb);

		RgbFmtFree(&m_rgb);
		m_rgb = NULL;
	}
}

AVFrame* CVideoPlayThread::PopSoundFrame()
{
	return m_prod->Pop();
}

void CVideoPlayThread::Play(RgbFmt* rgb)
{
	bool isplay = false;

	while (1)
	{
		if (m_time.clocktime == 0 && m_time.overtime == 0 && \
			m_time.pts == 0.0f && m_time.duration == 0.0f)
		{
			ForcePlay(rgb);
			break;
		}
		else
		{
			isplay = TryPlay(rgb);
			if (isplay)
				break;
			else
				Sleep(1);
		}

	}
}

bool CVideoPlayThread::TryPlay(RgbFmt* rgb)
{
	bool exist = false;
	int64_t syncpts =0, syncduration =0;
	bool isplay = false;

	m_media->SynchronousTime(&exist, &syncpts, &syncduration);

	if (exist)
		isplay = PlaySynchronousMySelf(rgb);
	else
		isplay = PlaySynchronous(rgb, syncpts, syncduration);

	return isplay;

}

bool CVideoPlayThread::CheckForcePlay(RgbFmt* rgb)
{
	int64_t clocktime = 0;
	const int64_t nexttime = (m_time.clocktime + m_time.duration - m_time.overtime);

	m_media->RealTime(&clocktime);

	return ((clocktime - nexttime) >= rgb->duration);
}

bool CVideoPlayThread::PlaySynchronousMySelf(RgbFmt* rgb)
{
	bool force = false, overtime = false;
	bool isplay = false;

	force = CheckForcePlay(rgb);
	if (force)
	{
		ForcePlay(rgb);
		isplay = true;
	}
	else
	{
		overtime = CheckCurrentTimeOver();
		if (overtime)
		{
			NormalPlay(rgb);
			isplay = true;
		}
		else
		{
			isplay = false;
		}
	}

	return isplay;
}

bool CVideoPlayThread::PlaySynchronous(RgbFmt* rgb, int64_t syncpts, int64_t syncduration)
{
	bool isplay = false;
	bool front = false, back = false;
	int64_t backtime = 0;

	TimeFrontBack(rgb,syncpts,syncduration,&front,&back,&backtime);

	if (!front && !back)
	{
		isplay = PlaySynchronousMySelf(rgb);
	}
	else
	{
		if (front)
		{
			//��֡
			DiscardFrame(rgb);
			isplay = true;
		}
		else
		{
			if (backtime < 10)
			{
				isplay = false;
			}
			else
			{
				DiscardFrame(rgb);
				isplay = true;
			}
		}
	}

	return isplay;
}

void CVideoPlayThread::DiscardFrame(RgbFmt* rgb)
{
	int64_t clocktime = 0;

	m_media->RealTime(&clocktime);

	m_time.clocktime = clocktime;
	m_time.overtime = 0;
	m_time.pts = rgb->pts * UnitMs();
	m_time.duration = rgb->duration * UnitMs();
}

RgbFmt* CVideoPlayThread::Convert(AVFrame* img)
{
	RgbFmt* rgb = ImageConvert( img, \
		m_sws, m_width, m_heigt, m_pixfmt);

	return rgb;
}

void CVideoPlayThread::TimeFrontBack(RgbFmt* rgb, int64_t syncPts, int64_t syncDuration, bool* outFront, bool* outBack, int64_t* outBacktime)
{
	const int64_t syncBegin = syncPts, syncEnd = syncPts + syncDuration;
	const int64_t rgbBegin = rgb->pts * UnitMs(), rgbEnd = (rgb->pts + rgb->duration) * UnitMs();

	const int64_t delta0 = syncBegin - rgbEnd;
	const int64_t delta1 = rgbBegin - syncEnd;
	
	
	*outFront = (delta0 >= 1);
	*outBack =  (delta1 >= 1);

	if (delta1 >= 1)
		*outBacktime = delta1;
	else
		*outBacktime = 0;
}

bool CVideoPlayThread::CheckCurrentTimeOver()
{
	int64_t clocktime = 0;
	const int64_t nexttime = (m_time.clocktime + m_time.duration - m_time.overtime);

	m_media->RealTime(&clocktime);

	return ((clocktime - nexttime) >= 0);
}

void CVideoPlayThread::ForcePlay(RgbFmt* rgb)
{
	int64_t clocktime = 0;

	m_media->RendererImage(rgb);

	m_media->RealTime(&clocktime);

	m_time.clocktime = clocktime;
	m_time.overtime = 0;
	m_time.pts = rgb->pts * UnitMs();
	m_time.duration = rgb->duration * UnitMs();

}

void CVideoPlayThread::NormalPlay(RgbFmt* rgb)
{
	int64_t clocktime = 0;
	const int nexttime = m_time.clocktime + m_time.duration - m_time.overtime;
	int64_t overtime = 0;

	m_media->RendererImage(rgb);

	m_media->RealTime(&clocktime);

	overtime = clocktime - nexttime;
	overtime = overtime >= 100 ? 0 : overtime;

	m_time.overtime = overtime;
	m_time.clocktime = clocktime;
	m_time.pts = rgb->pts * UnitMs();
	m_time.duration = rgb->duration * UnitMs();
}


double CVideoPlayThread::UnitMs()
{
	static const AVRational tb = m_media->StreamTimeBase(m_index);
	static const double ms = 1000.0f;
	static const double dtb = av_q2d(tb);
	static const double unit = dtb * ms;

	return unit;
}

