#pragma once

#include "Media/common.h"
#include "Thread/WorkThread.h"
#include "Thread/ProdCondProblem.h"
#include "Media/MediaInterface.h"

#include "Media/Sound.h"


class CAudioPlayThread : public CWorkThread
{
public:
	CAudioPlayThread(IMediaInterface* media, const int streamindex, const int maxcount);
	~CAudioPlayThread();

	void StartProcess();
	void PushSoundFrame(AVFrame* frm);	//call by other thread

	int64_t Pts() const;
	int64_t Duration() const;
protected:
	void RunProcess();

private:
	double UnitMs() ;

	void Play(SoundFmt* snd);

	void ForcePlay(SoundFmt* snd);
	void NormalPlay(SoundFmt* snd);

	bool CheckForcePlay(SoundFmt* snd);
	bool CheckCurrentTimeOver();

	bool TryPlay(SoundFmt* snd);

	void SaveInArray(SoundFmt* snd);

	
	void PlayArraySound();

	static void ClearArray(vector<SoundFmt*>& snds);
	static void ArrayDurationPts(vector<SoundFmt*>& snds,int64_t* outPts,int64_t* outDuration);
	

	AVFrame* PopSoundFrame();
	SoundFmt* Convert(AVFrame* frm);

private:
	IMediaInterface* m_media;
	const int m_index;
	const int m_maxCount;

	CProdCondProblem<AVFrame>* m_prod;

	AVFrame* m_frm;
	SoundFmt* m_sound;

	TimeInformation m_time;


	//
	vector<SoundFmt*> m_array;
};
