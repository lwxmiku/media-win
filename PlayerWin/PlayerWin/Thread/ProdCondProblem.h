#pragma once

#include <vector>
#include <algorithm>
#include <windows.h>
using namespace std;


template <typename product>
class CProdCondProblem
{

private:
	CONDITION_VARIABLE BufferNotEmpty;
	CONDITION_VARIABLE BufferNotFull;
	CRITICAL_SECTION   BufferLock;

	int max;
	vector<product*> prods;

public:
	CProdCondProblem(const int maxCount) {

		max = maxCount;
		InitializeConditionVariable(&BufferNotEmpty);
		InitializeConditionVariable(&BufferNotFull);

		InitializeCriticalSection(&BufferLock);
	}
	
	~CProdCondProblem(){

	}
	

public:
	void Push(product* prod)
	{

		int count = 0;

		EnterCriticalSection(&BufferLock);

		count = prods.size();
		if (count >= max)
		{
			SleepConditionVariableCS(&BufferNotFull, &BufferLock, INFINITE);
		}

		prods.push_back(prod);

		LeaveCriticalSection(&BufferLock);

		WakeConditionVariable(&BufferNotEmpty);

		/*
		int count = 0;

		//check is full
		mux.lock();
		count = prods.size();
		if (count >= max)
			notFull.wait(&mux);
		mux.unlock();

		//push into vector
		mux.lock();
		prods.push_back(prod);
		count = prods.size();
		mux.unlock();

		notEmpty.wakeAll();
		*/
	}

	product* Pop()
	{
		int count = 0;
		product* prod = NULL;

		EnterCriticalSection(&BufferLock);
		
		count = prods.size();
		if (count == 0)
		{
			SleepConditionVariableCS(&BufferNotEmpty, &BufferLock, INFINITE);
		}

		//pop 
		auto bit = prods.begin();
		prod = *bit;
		prods.erase(bit);

		count = prods.size();

		LeaveCriticalSection(&BufferLock);

		if (count < max)
			WakeConditionVariable(&BufferNotFull);

		return prod;


		/*
		int count = 0;
		product* prod = NULL;
		auto bit = prods.begin();

		//check is empty
		mux.lock();
		count = prods.size();
		if (count == 0)
			notEmpty.wait(&mux);
		mux.unlock();

		//pop from vector
		mux.lock();
		prod = prods.at(0);
		bit = prods.begin();
		prods.erase(bit);
		count = prods.size();
		mux.unlock();

		if (count < max)
			notFull.wakeAll();

		// end of function
		return prod;
		*/
	}

	void PopAll(vector<product*>* outpros)
	{


		auto bit = prods.begin(), eit = prods.end(), it = bit;
		product* pro = NULL;

		//mux.lock();
		EnterCriticalSection(&BufferLock);

		bit = prods.begin(), eit = prods.end();
		for (it = bit; it != eit; it++)
		{
			pro = *it;
			*it = NULL;
			outpros->push_back(pro);
		}

		prods.clear();

		//mux.unlock();
		LeaveCriticalSection(&BufferLock);
	}



};
